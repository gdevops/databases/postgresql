.. index::
   pair: Upgrade ; PostgreSQL

.. _postgresql_upgrade:

========================================================
PostgreSQL **upgrade**
========================================================

.. seealso::

   - https://www.2ndquadrant.com/en/blog/when-to-deploy-or-upgrade-to-a-new-major-postgresql-release/

.. toctree::
   :maxdepth: 3

   13/13
