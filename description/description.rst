

.. _postgresql_definition:

========================================================
PostgreSQL **definition**
========================================================

.. seealso::

   - https://wiki.postgresql.org/wiki/Postgres
   - https://www.postgresql.org/docs/current/history.html#AEN187
   - https://www.postgresql.org/message-id/473D7617.6070900@postgresql.org



Auteur: Dalibo Formation
=========================

.. seealso::

   - https://dalibo.com/formations

L’histoire de PostgreSQL est longue, riche et passionnante.
Au côté des projets libres Apache et Linux, PostgreSQL est l’un des plus vieux
logiciels libres en activité et fait partie des SGBD les plus sophistiqués à
l’heure actuelle.
