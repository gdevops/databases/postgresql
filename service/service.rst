.. index::
   pair: PostgreSQL ; service

.. _service_postgresql:

========================================================
PostgreSQL **service commands**
========================================================

sudo service postgresql status
=================================

::

    sudo service postgresql status


::

    ● postgresql.service - PostgreSQL RDBMS
       Loaded: loaded (/lib/systemd/system/postgresql.service; enabled; vendor preset: enabled)
       Active: active (exited) since Thu 2020-03-19 10:49:51 CET; 12min ago
      Process: 9343 ExecStart=/bin/true (code=exited, status=0/SUCCESS)
     Main PID: 9343 (code=exited, status=0/SUCCESS)

    mars 19 10:49:51 pvergain-MS-7721 systemd[1]: Starting PostgreSQL RDBMS...
    mars 19 10:49:51 pvergain-MS-7721 systemd[1]: Started PostgreSQL RDBMS.


sudo service postgresql stop
================================

Stop the database service.

::

    sudo service postgresql stop



sudo service postgresql restart
=================================

::

    sudo service postgresql restart



netstat -lp --protocol=unix | grep postgres
==============================================

::

    netstat -lp --protocol=unix | grep postgres

::

    (Tous les processus ne peuvent être identifiés, les infos sur les processus
    non possédés ne seront pas affichées, vous devez être root pour les voir toutes.)
    unix  2      [ ACC ]     STREAM     LISTENING     1258621  -                    /var/run/postgresql/.s.PGSQL.5432
