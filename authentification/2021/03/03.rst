.. index::
   pair: Authentication; SSL


========================================================
2021-03
========================================================


.. _ssl_2023_03_18:

2021-03-18 **Setting up SSL authentication for PostgreSQL**
=================================================================

- https://www.cybertec-postgresql.com/en/setting-up-ssl-authentication-for-postgresql/

PostgreSQL is a secure database and we want to keep it that way.

It makes sense, then, to consider SSL to encrypt the connection between
client and server.
This posting will help you to set up SSL authentication for PostgreSQL
properly, and hopefully also to understand some background information
to make your database more secure.


PostgreSQL is a secure database and we want to keep it that way.
It makes sense, then, to consider SSL to encrypt the connection between
client and server.
This posting will help you to set up SSL authentication for PostgreSQL
properly, and hopefully also to understand some background information
to make your database more secure.

Where does the second factor come in? You can add clientcert=verify-full
to another authentication method, such as the password-based scram-sha-256.
When you do this, your client has to provide both a valid certificate
AND password. Cool!

If you have a `public key infrastructure (PKI) <https://en.wikipedia.org/wiki/Public_key_infrastructure>`_ set up, you effectively
have a single-sign on system for your PostgreSQL databases.
You can then treat the password for the user in a local database as a
"second factor" for logging in. Again, cool!

Let's put this all together, and see how we can deploy a multifactor
single sign-on (SSO) authentication system for Postgres on Kubernetes
using cert-manager and PGO, the open source Postgres Operator from Crunchy Data!

