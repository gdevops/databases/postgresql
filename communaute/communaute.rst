.. index::
   pair: Community ; PostgreSQL
   ! Community

.. _postgresql_community:

=============================
**PostgreSQL community**
=============================


Forum
======

- https://forums.postgresql.fr/


Wiki
=====

- https://wiki.postgresql.org/wiki/Main_Page
- https://wiki.postgresql.org/wiki/Main_Page/fr


Dalibo
=========

- https://github.com/dalibo
- https://x.com/dalibo
- https://x.com/pgsession
- https://www.youtube.com/c/Dalibo/videos
- https://www.dalibo.com/formations
- https://pgsessions.com/archives.html
