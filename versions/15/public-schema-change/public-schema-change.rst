.. index::
   pair: PostgreSQL 15 ; Public schema change


.. _public_schema_change:

========================================================
**Public schema change in PostgreSQL 15**
========================================================


TL;DR
=======

**Consider creating specific schemas for applications and give out specific
privileges as required.**


Essai de reformulation en français
---------------------------------------

Avant la version PostgreSQL 15 (2022-10-13) , quand on créait une base de données,
celle-ci était crée dans le schéma dit 'public' et tout le monde avait la
possibilité de la modifier.

Depuis PostgreSQL 15, un changement fondamental s'est produit. Pour des
raisons de sécurité, il est recommandé:

- 1) de créer un rôle pour la création de la base
- 2) de créer un schéma ayant le nom du rôle


.. _passage_schema_public_a_intranet:

Exemple de passage d'un schéma **public** à un schéma **intranet**
======================================================================

Sauvegarde avec pg_dump
-------------------------

On exporte avec l'option **format=plain**

::

    date; export PGPASSWORD=${PGPASSWORD_PRODUCTION}; pg_dump -v -h databasehost -U intranet --format=plain database_name > ~/Documents/database_name_${TODAY}.sql; date



Modifications du fichier ~/Documents/database_name_${TODAY}.sql
-----------------------------------------------------------------

Modification du fichier ~/Documents/database_name_${TODAY}.sql::

    sed -i "s/^ALTER SCHEMA public OWNER TO postgres;/SET search_path = 'intranet';/" $1
    sed -i 's/public[.]/intranet./g' $1
    sed -i 's/Schema: public/Schema: intranet/g' $1
    sed -i 's/REVOKE USAGE ON SCHEMA public FROM PUBLIC;/ /' $1
    sed -i 's/GRANT ALL ON SCHEMA public TO PUBLIC;/ /' $1

Création de la base db_intranet, du rôle intranet et du schéma 'intranet'
---------------------------------------------------------------------------

::

    psql -U intranet -h localhost -p 5433
    CREATE ROLE intranet;
    ALTER ROLE intranet WITH SUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS;
    ALTER ROLE intranet WITH LOGIN PASSWORD 'password';
    CREATE DATABASE database_name WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'fr_FR.utf8' LC_CTYPE = 'fr_FR.utf8';
    create schema intranet
    ALTER ROLE intranet SET search_path = intranet;


Vérification avec la commande :ref:`\drds <drds_command>`

::

    psql -U intranet -h localhost -p 5433 -c "\drds"

::

    db_intranet=> \drds
                   Liste des paramètres
       Rôle   | Base de données |       Réglages
    ----------+-----------------+----------------------
     intranet |                 | search_path=intranet



Import dans la base de données db_intranet, dans le schéma intranet
------------------------------------------------------------------------

Sur la base de données locale::

    import_local_today:
        date; psql -U intranet -h localhost -p 5433 -c "drop schema intranet cascade" ; date
        date; psql -U intranet -h localhost -p 5433 -c "create schema intranet" ; date
        date; psql -U intranet -h localhost -p 5433 -c "\drds" ; date
        date; psql -U intranet -h localhost -p 5433 < ~/Documents/db_intranet_${TODAY}.sql; date

Sur la base de données staging::

    import_staging_today:
        date; export PGPASSWORD=${PGPASSWORD_STAGING}; psql -h database-staging.srv.int.id3.eu -c "drop schema intranet cascade" ; date
        date; export PGPASSWORD=${PGPASSWORD_STAGING}; psql -h database-staging.srv.int.id3.eu -c "create schema intranet" ; date
        date; export PGPASSWORD=${PGPASSWORD_STAGING}; psql -h database-staging.srv.int.id3.eu -c "\drds" ; date
        date; export PGPASSWORD=${PGPASSWORD_STAGING}; psql -h database-staging.srv.int.id3.eu < ~/Documents/db_intranet_${TODAY}.sql; date


Lists tables, views and sequences with their associated access privileges
----------------------------------------------------------------------------

- :ref:`dp_command`

::

    psql -U intranet -h localhost -p 5433 -c "\dp"

::

                                                            Droits d'accès
      Schéma  |                      Nom                      |   Type   | Droits d'accès | Droits d'accès à la colonne | Politiques
    ----------+-----------------------------------------------+----------+----------------+-----------------------------+------------
     intranet | article                                       | table    |                |                             |
     intranet | article_achat                                 | table    |                |                             |
     intranet | article_achat_id_seq                          | séquence |                |                             |
     intranet | article_codification                          | table    |                |                             |
     intranet | article_codification_id_seq                   | séquence |                |                             |
     intranet | article_content_commentaire                   | table    |                |                             |
     intranet | article_content_commentaire_id_seq            | séquence |                |                             |




https://www.enterprisedb.com/blog/new-public-schema-permissions-postgresql-15, par Peter Eisentraut
=======================================================================================================

- https://www.enterprisedb.com/blog/new-public-schema-permissions-postgresql-15

One of the changes in PostgreSQL 15 is a new default set of permissions
on the public schema. The details of this are subtle.

**Each database contains a schema named public by default**.

This schema is also by default part of the search_path.

The effect is that if you don't specify any schema in any commands, **all
the activities take place in this public schema**.

If you don't care about schemas, then this allows you to use the database
as if tables are directly inside a database, **as it was before schemas
were added to PostgreSQL (version 7.3!)**.

In fact, this whole setup was a carefully calibrated dance to make the
transition from before-schemas to schemas seamless

As part of this setup, the public schema also has a set of default permissions:
It has the USAGE privilege granted to PUBLIC (meaning "everyone", which
is unrelated to the equally named schema), which allows anyone to
refer to objects in the schema.

And it has the CREATE privilege granted, which allows anyone to create
objects in the schema.
The latter one is the problematic one.
If anyone can create objects in the schema, then you can trick others
into using your object instead of one in the system catalogs (pg_catalog schema).
This was the subject of CVE-2018-1058. The workarounds described for this
were basically elaborate versions of "don't do that" or "be careful",
but a better default setup proved elusive so far.

We could just not grant the CREATE privilege by default. Then the database
owner could grant the CREATE privilege explicitly themselves if they want
to.
This would be one extra step for a bit more security. However, if you
look carefully into a PostgreSQL 14 (or earlier) installation, the public
schema is actually owned by the database bootstrap user (usually called
postgres).
But databases can each have owners that are not the bootstrap user or
superusers at all.
This is because when you create a database in PostgreSQL, it actually
copies the template database, and at that point it doesn't know who the
database owner will be. So a non-superuser database owner will see
their newly minted database and find in it a schema named public owned
by some other user named postgres, and so they cannot do any GRANT
statements on that schema!

The fix here required a sequence of changes:

- There is **a new role pg_database_owner**, which implicitly has the actual
  owner of the current database as a member.
- The public schema is owned by that role now.
- The public schema does not have CREATE privileges granted by default anymore.

So now, by default, unprivileged users cannot lay traps in the public
schema anymore. But database owners now have the full capability to adjust
these privileges as they see fit.

Note that upgrading from a previous version to PostgreSQL 15 (using pg_dump
or pg_upgrade) preserves the original privileges, so you will not automatically
get this more secure setup. To fix up upgraded databases, run these commands:

::

    ALTER SCHEMA public OWNER TO pg_database_owner;
    REVOKE CREATE ON SCHEMA public FROM PUBLIC;

Going forward, perhaps reconsider your use of the public schema, except
for casual use.
**Consider creating specific schemas for applications and give out specific
privileges as required.**


https://www.cybertec-postgresql.com/en/error-permission-denied-schema-public/
================================================================================

- https://www.cybertec-postgresql.com/en/error-permission-denied-schema-public/

**In PostgreSQL 15, a fundamental change took place** which is relevant to
every user who happens to work with permissions:
**The default permissions of the public schema have been modified**.

This is relevant because it might hurt you during application deployment.
You need to be aware of how it may affect you.

Creating users
---------------

Many people work as superusers only.
This is not recommended and can lead to serious security issues.
It’s recommended to create separate users to run your application.

In PostgreSQL you can create a new user using the CREATE USER or the
CREATE ROLE command.
The difference between these two options is that CREATE USER sets the
LOGIN privilege directly while CREATE ROLE will set this attribute to NOLOGIN.

In this example, you’ll create a “demo” user, as shown in the next listing::

    security=# CREATE USER demo LOGIN;
    CREATE ROLE


Using the PUBLIC schema in PostgreSQL 15
--------------------------------------------

In PostgreSQL 14 and in prior versions, creating a table would be possible.
The new table would simply end up in the public schema and all would be good.
The problem with this approach is that the public schema can quickly
turn into a trashcan containing all kinds of used and unused tables –
which is neither desirable nor recommended. Therefore PostgreSQL has
made a major leap forward and changed this behavior.


https://www.depesz.com/2021/09/10/waiting-for-postgresql-15-revoke-public-create-from-public-schema-now-owned-by-pg_database_owner/
======================================================================================================================================

- https://www.depesz.com/2021/09/10/waiting-for-postgresql-15-revoke-public-create-from-public-schema-now-owned-by-pg_database_owner/

**This is big change.**

PostgreSQL always allowed, by default, any user to connect to any database,
and create new tables (and views, and so on) in “public" schema.
The one that is created by default.

Some people saw it as security issue, some didn't.

Some deleted public schema, or just tightened privileges.

Not any more. Since Pg 15 (assuming the change will not get rolled back),
by default, non-superuser accounts will not be able to create tables
in public schema of databases they don't own.

Let's see it:


https://fluca1978.github.io/2022/07/15/PostgreSQL15PublicSchema.html
=======================================================================

- https://fluca1978.github.io/2022/07/15/PostgreSQL15PublicSchema.html


In PostgreSQL 15 the default public schema that every database has will
have a different set of permissions.

In fact, before PostgreSQL 15, every user could manipulate the public
schema of a database he is not owner.

Since the upcoming new version, only the database owner will be granted
full access to the public schema, while other users will need to get an
explicit GRANT:

Imagine the user luca is owner of the database testdb: it means he can
do whatever he wants on the database.


https://www.crunchydata.com/blog/be-ready-public-schema-changes-in-postgres-15
=================================================================================

- https://www.crunchydata.com/blog/be-ready-public-schema-changes-in-postgres-15


Separating User Data
-----------------------

Eventually you will have production data, and need to limit access to
that data, but retain temporary and experimental data and users.

How can you easily segment that scratch data from production data?

PostgreSQL makes it very easy to keep user data separate, because the
default search_path starts with a "user schema".

See that $user entry in the search_path? It's a dynamic variable that
gets filled in with your user name.


PostgreSQL makes it very easy to keep user data separate, because the
default search_path starts with a "user schema".

See that $user entry in the search_path? It's a dynamic variable that
gets filled in with your user name.

::

    postgres=> show search_path;

::

       search_path
    -----------------
     "$user", public


    To try it out, first make a basic login user.

    -- as 'postgres'
    CREATE USER intranet;

    Then, make a "user schema" for that user.

    -- as 'postgres'
    CREATE SCHEMA intranet AUTHORIZATION intranet;

Now, when intranet creates a table without specifying the schema, **the
table will be created in the first schema on their search_path.**

By default, the first schema is $user which expands to their user name,
and there is now a schema that matches their user name, and thus the
place the table is created!


https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1058
===============================================================

- https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1058

A flaw was found in the way Postgresql allowed a user to modify the
behavior of a query for other users.

An attacker with a user account could use this flaw to execute code
with the permissions of superuser in the database.

Versions 9.3 through 10 are affected.


https://git.postgresql.org/gitweb/?p=postgresql.git;a=commitdiff;h=b073c3ccd06e4cb845e121387a43faa8c68a7b62
==============================================================================================================

- https://git.postgresql.org/gitweb/?p=postgresql.git;a=commitdiff;h=b073c3ccd06e4cb845e121387a43faa8c68a7b62
- https://www.postgresql.org/message-id/20201031163518.GB4039133@rfd.leadboat.com

This switches the default ACL to what the documentation has recommended
since CVE-2018-1058.  Upgrades will carry forward any old ownership and
ACL.  

Sites that declined the 2018 recommendation should take a fresh look.  

Recipes for commissioning a new database cluster from scratch may
need to create a schema, grant more privileges, etc.  

Out-of-tree test suites may require such updates.

Reviewed by Peter Eisentraut.

Discussion: https://postgr.es/m/20201031163518.GB4039133@rfd.leadboat.com


https://andreas.scherbaum.la/blog/archives/1120-Changes-to-the-public-schema-in-PostgreSQL-15-and-how-to-handle-upgrades.html
=================================================================================================================================

- https://andreas.scherbaum.la/blog/archives/1120-Changes-to-the-public-schema-in-PostgreSQL-15-and-how-to-handle-upgrades.html

In September 2021, a patch for the upcoming PostgreSQL version 15 was
`committed <https://git.postgresql.org/gitweb/?p=postgresql.git;a=commitdiff;h=b073c3ccd06e4cb845e121387a43faa8c68a7b62>`_
which introduces a visible change for users: the CREATE privilege for
the public schema is no longer set by default.

This is a recommendation from `CVE-2018-1058 <https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1058>`_

::

    SELECT version();

::

    ----------------------------------------------------------------------------------------------------------------------------
    PostgreSQL 15.1 (Debian 15.1-1.pgdg110+1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 10.2.1-6) 10.2.1 20210110, 64-bit
    (1 ligne)
