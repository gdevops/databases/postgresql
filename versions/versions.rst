.. index::
   pair: Versions ; PostgreSQL

.. _postgresl_versions:

========================================================
PostgreSQL versions
========================================================

- https://www.postgresql.org/about/featurematrix/
- https://pgpedia.info/postgresql-versions/index.html
- https://www.postgresql.org/docs/release/
- https://www.postgresql.org/list/pgsql-announce/
- https://www.postgresql.org/developer/core/
- https://en.wikipedia.org/wiki/PostgreSQL#Release_history


.. figure:: images/evolution_to_17.png
   :align: center

   https://www.postgresql.fastware.com/blog/discussing-postgresql-what-changes-in-version-16

.. toctree::
   :maxdepth: 3

   18/18
   17/17
   16/16
   15/15
   14/14
   13.4/13.4
   13/13
   12.8/12.8
   12/12
   11/11
   7.3/7.3
