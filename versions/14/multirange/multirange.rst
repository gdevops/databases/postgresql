.. index::
   pair: PostgreSQL 14; multirange
   ! multirange


.. _multirange:

================================================================================================================
**multirange** A data type for storing multiple non-overlapping ranges
================================================================================================================

- https://pgpedia.info/m/multirange.html
- https://git.postgresql.org/gitweb/?p=postgresql.git;a=commit;h=6df7a9698bb036610c1e8c6d375e1be38cb26d5f
- https://www.depesz.com/2020/12/21/waiting-for-postgresql-14-multirange-datatypes/

Description
=============

**A data type for storing multiple non-overlapping ranges**

multirange is a data type for storing multiple non-overlapping ranges.

The multirange data type was added in PostgreSQL 14..
