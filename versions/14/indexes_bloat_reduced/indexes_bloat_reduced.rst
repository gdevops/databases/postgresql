

.. _index_bloat_reduced:

==================================================================================================================================
Index bloat reduced in PostgreSQL v14
==================================================================================================================================

- https://www.cybertec-postgresql.com/en/index-bloat-reduced-in-postgresql-v14/


Introduction
==============


PostgreSQL v12 brought more efficient storage for indexes, and v13 improved
that even more by adding deduplication of index entries.

But Peter Geoghegan is not done yet !

PostgreSQL v14 will bring “bottom-up” index entry deletion, which is
targeted at reducing unnecessary page splits, index bloat and fragmentation
of heavily updated indexes.

How does v14 reduce index bloat even further ?
=================================================

“Bottom-up index tuple deletion” goes farther than the previous approaches:
it deletes index entries that point to dead tuples right before an index
page split is about to occur.

This can reduce the number of index entries and avoid the expensive
page split, together with the bloat that will occur later, when
VACUUM cleans up.

In a way, this performs part of the work of VACUUM earlier, at a point
where it is useful to avoid index bloat.


A test case
===============

To demonstrate the effects of the new feature, I performed a custom
pgbench run on PostgreSQL v13 and v14.

This is the table for the test:

.. code-block:: sql

    CREATE TABLE testtab (
       id bigint CONSTRAINT testtab_pkey PRIMARY KEY,
       val integer
    );

    INSERT INTO testtab
       SELECT i, 0 FROM generate_series(1, 10000) AS i;

    CREATE INDEX testtab_val_idx ON testtab (val);

The index that we will focus on is testtab_pkey.

This is the pgbench script called “bench.sql”

.. code-block:: bash

    \set id random_gaussian(1, 10000, 10)
    UPDATE testtab SET val = val + 1 WHERE id = :id;
    UPDATE testtab SET val = val + 1 WHERE id = :id;
    UPDATE testtab SET val = val + 1 WHERE id = :id;
    UPDATE testtab SET val = val + 1 WHERE id = :id;
    UPDATE testtab SET val = val + 1 WHERE id = :id;
    UPDATE testtab SET val = val + 1 WHERE id = :id;
    UPDATE testtab SET val = val + 1 WHERE id = :id;
    UPDATE testtab SET val = val + 1 WHERE id = :id;
    UPDATE testtab SET val = val + 1 WHERE id = :id;
    UPDATE testtab SET val = val + 1 WHERE id = :id;

I chose a normal distribution, because in real life there are usually
some (recent?) table rows that receive more updates than others.

The row is updated ten times, in order to make it more likely that the
affected page will have to be split.

I run the script 60000 times (10000 iterations by 6 clients) as follows::

    pgbench -n -c 6 -f bench.sql -t 10000 test


Comparing the test results

We use the **pgstattuple extension** to get index statistics with psql:

SELECT * FROM pgstatindex('testtab_pkey') \gx

This is what we get for v13::

    ─[ RECORD 1 ]──────┬───────
    version            │ 4
    tree_level         │ 1
    index_size         │ 319488
    root_block_no      │ 3
    internal_pages     │ 1
    leaf_pages         │ 37
    empty_pages        │ 0
    deleted_pages      │ 0
    avg_leaf_density   │ 66.6
    leaf_fragmentation │ 24.32

For v14, the result is::

    ─[ RECORD 1 ]──────┬───────
    version            │ 4
    tree_level         │ 1
    index_size         │ 245760
    root_block_no      │ 3
    internal_pages     │ 1
    leaf_pages         │ 28
    empty_pages        │ 0
    deleted_pages      │ 0
    avg_leaf_density   │ 87.91
    leaf_fragmentation │ 0

The size of the index is 23% less in v14.

The density (the percentage of available space that is used) is as
good as for a new index, if you consider the default fillfactor of 90.

For the other index, we can also observe a small improvement, but far
less than for the primary key index.


Will I be able to use this feature after a pg_upgrade ?
========================================================

The storage format of the index is unchanged, so this will automatically
work after a pg_upgrade of an index created on PostgreSQL v12 or later.

**If the index was created with an earlier version of PostgreSQL, you will
have to REINDEX the index to benefit from the new feature**.

Remember that pg_upgrade simply copies the index files and does not
update the internal index version.


Conclusion
===========

PostgreSQL v14 continues to bring improvements to **B-tree indexes.**

While this particular one may not be revolutionary, it promises to provide
a solid improvement for many workloads, especially those with lots of updates.
