.. index::
   pair: PostgreSQL 14; date_bin
   ! date_bin


.. _date_bin:

================================================================================================================
**date_bin function** A function for converting a timestamp to the start of the nearest specified interval
================================================================================================================

- https://pgpedia.info/d/date_bin.html
- https://www.depesz.com/2021/03/31/waiting-for-postgresql-14-add-date_bin-function/
- https://git.postgresql.org/pg/commitdiff/49ab61f0bdc93984a8d36b602f6f2a15f09ebcc7


Description
=============

- https://www.hagander.net/talks/PostgreSQL%2014.pdf


- Group" timestamp into a "bin"
- A bit like date_trunc But much more granular!



Usage
==========

::

    date_bin (stride INTERVAL, source TIMESTAMP, origin TIMESTAMP) → TIMESTAMP

    date_bin (stride INTERVAL, source TIMESTAMP WITH TIME ZONE, origin TIMESTAMP WITH TIME ZONE) → TIMESTAMP WITH TIME ZONE


Examples
===========

::

    psql -h 127.0.0.1 -U postgres

::

    postgres=# SELECT date_bin('15 minutes', now(), '2021-01-01');
    2021-09-08 22:30:00+02

    postgres=# SELECT date_bin('17 minutes 30 seconds', now(), '2021-01-01');
