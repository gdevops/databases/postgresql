.. index::
   pair: PostgreSQL; connexion
   triple: port; netstat; 5432
   ! Linux admin

.. _postgresql_connexion:

========================================================
**Postgresql connexion**
========================================================

- :ref:`service_postgresql`
- :ref:`pg_environment_variables`

The more useful command
============================

::

    psql postgresql://<user>:<password>@<host>:<port>/<database_name>


Example::

    psql postgresql://intranet:<password>@database-host:5432/db_intranet


Quel est le port par défaut d'une instance PostgreSQL ?
===========================================================

Le port par défaut de PostgreSQL est 5432 mais si d'autres anciennes
configurations existent (sous /etc/postgresql) alors les autres
numéros seront 5433, 5434, etc...

Pour connaitre le port de connexion on peut employer la commande
**netstat**

::

    netstat -nl | grep postgres

::

    root@uc045:/etc/apt/sources.list.d# netstat -nl | grep postgres
    unix  2      [ ACC ]     STREAM     LISTENING     23458    /var/run/postgresql/.s.PGSQL.5432


.. _user_admin_login:

Connexion en tant qu'utilisateur linux 'postgres' (Database Administrator)
===============================================================================

::

    sudo -u postgres psql

::

    psql (15.3 (Debian 15.3-1.pgdg120+1))
    Saisissez « help » pour l'aide.

    postgres=#


Connexion en tant que user linux normal (Après modification de pg_hba.conf)
==============================================================================

- :ref:`update_pg_hba_conf_2023_08_04`


Connexion avec le rôle postgres (-U postgres)
----------------------------------------------
::

    psql -U postgres

::

    psql (15.3 (Debian 15.3-1.pgdg120+1))
    Saisissez « help » pour l'aide.

    postgres=#


.. warning:: On veille à ce qu'aucune variable d'environnement PostgreSQL
   ne soit positionnée.

   - :ref:`pg_environment_variables`
