.. index::
   pair: Partition ; Pruning


.. _partition_pruning:

========================================================
Partition pruning
========================================================

.. seealso::

   - https://www.postgresql.org/docs/11/static/runtime-config-query.html#GUC-ENABLE-PARTITION-PRUNING
   - https://www.postgresql.org/docs/11/static/ddl-partitioning.html#DDL-PARTITION-PRUNING




enable_partition_pruning (boolean)
====================================

Enables or disables the query planner's ability to eliminate a partitioned
table's partitions from query plans.

This also controls the planner's ability to generate query plans which
allow the query executor to remove (ignore) partitions during query
execution.

The default is on. See Section `5.10.4`_ for details.


.. _`5.10.4`:  https://www.postgresql.org/docs/11/static/ddl-partitioning.html#DDL-PARTITION-PRUNING

DDL-PARTITION-PRUNING
=======================

.. seealso::

   - https://www.postgresql.org/docs/11/static/ddl-partitioning.html#DDL-PARTITION-PRUNING


By using the EXPLAIN command and the enable_partition_pruning configuration
parameter, it's possible to show the difference between a plan for which
partitions have been pruned and one for which they have not.
