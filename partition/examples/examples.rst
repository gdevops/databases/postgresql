.. index::
   pair: Partition PostgreSQL; Exemples


.. _ex_partition_postgres:

========================================================
Exemples de partition PostgreSQL
========================================================

.. toctree::
   :maxdepth: 3

   logface/logface
   pgdash/pgdash
