.. index::
   pair: Partition PostgreSQL; pgdash
   pair: PostgreSQL; 11
   pair: PostgreSQL ; Foreign Key
   pair: PostgreSQL ; Unique Indexes

.. _ex_partition_pgdash:

========================================================
Exemples de partition PostgreSQL
========================================================

.. seealso::

   - https://pgdash.io/blog/partition-postgres-11.html




Create the partition
===========================

::

    CREATE TABLE measurement (
        logdate         date not null,
        peaktemp        int,
        unitsales       int
    ) PARTITION BY RANGE (logdate);


::

    CREATE TABLE measurement_y2016 PARTITION OF measurement
    FOR VALUES FROM ('2016-01-01') TO ('2017-01-01');

    CREATE TABLE measurement_y2017 PARTITION OF measurement
    FOR VALUES FROM ('2017-01-01') TO ('2018-01-01');

    INSERT INTO measurement (logdate, peaktemp, unitsales)
        VALUES ('2016-07-10', 66, 100); -- goes into measurement_y2016 table


Update the table
===================


::

    pg11=# UPDATE measurement SET logdate='2017-07-10';



::

    pg11=# select * from measurement_y2017;

::


      logdate   | peaktemp | unitsales
    ------------+----------+-----------
     2017-07-10 |       66 |       100
    (1 row)



Create Default Partitions
=============================

With v11 it is now possible to create a “default” partition, which can
store rows that do not fall into any existing partition’s range or list.

::

    pg11=# CREATE TABLE measurement_default PARTITION OF measurement DEFAULT;
    CREATE TABLE



Automatic Index Creation
============================

In v11, if you create an index on the parent table, Postgres will
automatically create indexes on all the child tables:

::

    pg11=# CREATE INDEX ixsales ON measurement(unitsales);

::

    CREATE INDEX

::

    pg11=# \d measurement_y2016


::


               Table "public.measurement_y2016"
      Column   |  Type   | Collation | Nullable | Default
    -----------+---------+-----------+----------+---------
     logdate   | date    |           | not null |
     peaktemp  | integer |           |          |
     unitsales | integer |           |          |


    Partition of: measurement FOR VALUES FROM ('2016-01-01') TO ('2017-01-01')

    Indexes:
        "measurement_y2016_unitsales_idx" btree (unitsales)

**Any new partitions created after the index was created, will also
automagically get an index added to it**.



Foreign Key Support
=========================


In v11, foreign keys are allowed:

::

    pg11=# CREATE TABLE invoices ( invoice_id integer PRIMARY KEY );

::

    CREATE TABLE

::

    pg11=# CREATE TABLE sale_amounts_1 (
    pg11(#     saledate   date    NOT NULL,
    pg11(#     invoiceid  integer REFERENCES invoices(invoice_id)
    pg11(# ) PARTITION BY RANGE (saledate);

::

    CREATE TABLE


Unique Indexes
=================

With Postgres 11, you can create a unique index on the master:

::

    pg11=# CREATE TABLE sale_amounts_2 (
    pg11(#     saledate   date NOT NULL,
    pg11(#     invoiceid  INTEGER,
    pg11(#     UNIQUE (saledate, invoiceid)
    pg11(# ) PARTITION BY RANGE (saledate);

::

    CREATE TABLE

and Postgres will take care of creating indexes on all existing and
future child tables:

::

    pg11=# CREATE TABLE sale_amounts_2_y2016 PARTITION OF sale_amounts_2
    pg11-# FOR VALUES FROM ('2016-01-01') TO ('2017-01-01');

::

    CREATE TABLE

::

    pg11=# \d sale_amounts_2_y2016

::

             Table "public.sale_amounts_2_y2016"
      Column   |  Type   | Collation | Nullable | Default
    -----------+---------+-----------+----------+---------
     saledate  | date    |           | not null |
     invoiceid | integer |           |          |
    Partition of: sale_amounts_2 FOR VALUES FROM ('2016-01-01') TO ('2017-01-01')
    Indexes:
        "sale_amounts_2_y2016_saledate_invoiceid_key" UNIQUE CONSTRAINT, btree (saledate, invoiceid)


The columns in the index definition should be a superset of the partition
key columns. This means that the uniqueness is enforced locally to each
partition, and you can’t use this to enforce uniqueness of
alternate-primary-key columns.


Partition-level Aggregation
==============================

::

    SET enable_partitionwise_aggregate=on;

::

    SET

::

    pg11=# EXPLAIN SELECT logdate, count(*) FROM measurement GROUP BY logdate;

::

                                       QUERY PLAN
    ---------------------------------------------------------------------------------
     Append  (cost=7.49..38.96 rows=1098 width=12)
       ->  HashAggregate  (cost=7.49..11.15 rows=366 width=12)
             Group Key: measurement_y2016.logdate
             ->  Seq Scan on measurement_y2016  (cost=0.00..5.66 rows=366 width=4)
       ->  HashAggregate  (cost=7.49..11.14 rows=365 width=12)
             Group Key: measurement_y2017.logdate
             ->  Seq Scan on measurement_y2017  (cost=0.00..5.66 rows=366 width=4)
       ->  HashAggregate  (cost=7.51..11.18 rows=367 width=12)
             Group Key: measurement_default.logdate
             ->  Seq Scan on measurement_default  (cost=0.00..5.67 rows=367 width=4)
    (10 rows)


Now the grouping happens once per partition, and the results are just
concatenated (we’re grouping by the partition key here).

Pushing down the aggregation should result in faster queries because of
**better parallelism and improved lock handling**.
