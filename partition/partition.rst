.. index::
   pair: Partition ; PostgreSQL
   pair: 11 ; PostgreSQL


.. _partition_postgres_log:

========================================================
Partition PostgreSQL
========================================================

.. seealso::

   - https://www.postgresql.org/docs/11/static/ddl-partitioning.html



Introduction
===============

PostgreSQL 11, due to be released later this year (2018), comes with a
bunch of improvements for the declarative partitioning feature that was
introduced in version 10.


Partition pruning
===================

.. toctree::
   :maxdepth: 3

   partition_pruning/partition_pruning


Exemples
===========

.. toctree::
   :maxdepth: 3


   examples/examples
