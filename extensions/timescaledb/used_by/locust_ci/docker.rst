
.. _locust_timescaledb_installation:

============================================================================================================================
|TimeScaleDB| **locust-ci Docker TimescaleDB** installation
============================================================================================================================


Dashboard locust-timescaledb installation
============================================

- https://github.com/SvenskaSpel/locust-plugins/tree/master/locust_plugins/dashboards
- https://github.com/SvenskaSpel/locust-plugins/tree/master/locust_plugins/dashboards/locust-timescale

1. Getting started: https://docs.timescale.com/timescaledb/latest/getting-started
2. API reference documentation: https://docs.timescale.com/api/latest
3. How TimescaleDB is designed: https://docs.timescale.com/timescaledb/latest/overview/core-concepts

::

    locust-ci-postgres-1  | WELCOME TO
    locust-ci-postgres-1  |  _____ _                               _     ____________
    locust-ci-postgres-1  | |_   _(_)                             | |    |  _  \ ___ \
    locust-ci-postgres-1  |   | |  _ _ __ ___   ___  ___  ___ __ _| | ___| | | | |_/ /
    locust-ci-postgres-1  |   | | | |  _ ` _ \ / _ \/ __|/ __/ _` | |/ _ \ | | | ___ \
    locust-ci-postgres-1  |   | | | | | | | | |  __/\__ \ (_| (_| | |  __/ |/ /| |_/ /
    locust-ci-postgres-1  |   |_| |_|_| |_| |_|\___||___/\___\__,_|_|\___|___/ \____/
    locust-ci-postgres-1  |                Running version 2.8.1
    locust-ci-postgres-1  | For more information on TimescaleDB, please visit the following links:
    locust-ci-postgres-1  |
    locust-ci-postgres-1  |  1. Getting started: https://docs.timescale.com/timescaledb/latest/getting-started
    locust-ci-postgres-1  |  2. API reference documentation: https://docs.timescale.com/api/latest
    locust-ci-postgres-1  |  3. How TimescaleDB is designed: https://docs.timescale.com/timescaledb/latest/overview/core-concepts
    locust-ci-postgres-1  |

Dockerfile
-------------

.. literalinclude:: locust-timescale/Dockerfile
   :linenos:

timescale_schema.sql
--------------------------

.. literalinclude:: locust-timescale/timescale_schema.sql
   :linenos:

zz_hypertable.sql
--------------------------

.. literalinclude:: locust-timescale/zz_hypertable.sql
   :linenos:


Makefile
--------------------------

.. literalinclude:: locust-timescale/Makefile
   :linenos:
