.. index::
   pair: PostgreSQL  extension; TimescaleDB


.. _timescaledb:

============================================================================================================================
|TimeScaleDB| **TimescaleDB** (An open-source time-series SQL database optimized for fast ingest and complex queries)
============================================================================================================================

- https://github.com/timescale
- https://github.com/timescale/timescaledb
- https://github.com/timescale/timescaledb/commits.atom
- https://blog.timescale.com/blog/top-5-postgresql-extensions/

.. toctree::
   :maxdepth: 3

   articles/articles
   installation/installation
   used_by/used_by
