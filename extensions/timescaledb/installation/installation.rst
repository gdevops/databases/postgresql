.. index::
   pair: Installation; PostgreSQL TimescaleDB


.. _timescaledb_installation:

===============================================================================
|TimeScaleDB| **PostgreSQL TimescaleDB installation**
===============================================================================

- https://docs.timescale.com/self-hosted/latest/install/
- https://docs.timescale.com/self-hosted/latest/install/installation-linux
- https://docs.timescale.com/self-hosted/latest/install/installation-linux/#set-up-the-timescaledb-extension
- https://docs.timescale.com/timescaledb/latest/how-to-guides/configuration/telemetry


Install TimescaleDB |TimeScaleDB| on GNU/Linux Debian
==========================================================

- https://docs.timescale.com/self-hosted/latest/install/
- https://docs.timescale.com/self-hosted/latest/install/installation-linux/

TimescaleDB is available pre-packaged for several platforms (Linux, Docker,
MacOS, Windows). More information can be found in our documentation.


Step 1 At the command prompt, as root, add the PostgreSQL third party repository to get the latest PostgreSQL packages
--------------------------------------------------------------------------------------------------------------------------

At the command prompt, as root, add the PostgreSQL third party repository
to get the latest PostgreSQL packages:

::

    sudo apt install gnupg postgresql-common apt-transport-https lsb-release wget


step 2 Run the PostgreSQL repository setup script
-------------------------------------------------------

::

    ✦ ❯ sudo /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh

::

    This script will enable the PostgreSQL APT repository on apt.postgresql.org on
    your system. The distribution codename used will be bullseye-pgdg.

    Press Enter to continue, or Ctrl-C to abort.

    Using keyring /usr/share/postgresql-common/pgdg/apt.postgresql.org.gpg
    Writing /etc/apt/sources.list.d/pgdg.sources ...
    '/etc/apt/sources.list.d/pgdg.list' supprimé
    '/etc/apt/trusted.gpg.d/apt.postgresql.org.gpg' supprimé

    Running apt-get update ...
    Réception de :1 http://packages.microsoft.com/repos/code stable InRelease [3 569 B]
    Atteint :2 https://deb.debian.org/debian-security bullseye-security InRelease
    Atteint :3 http://deb.debian.org/debian bullseye InRelease
    Atteint :4 https://updates.signal.org/desktop/apt xenial InRelease
    Réception de :5 http://deb.debian.org/debian bullseye-updates InRelease [44,1 kB]
    Atteint :7 https://download.docker.com/linux/debian bullseye InRelease
    Ign :8 https://repo.vivaldi.com/stable/deb stable InRelease
    Atteint :9 https://repo.vivaldi.com/stable/deb stable Release
    Réception de :10 http://packages.microsoft.com/repos/code stable/main arm64 Packages [74,5 kB]
    Réception de :11 http://packages.microsoft.com/repos/code stable/main amd64 Packages [74,1 kB]
    Réception de :12 http://packages.microsoft.com/repos/code stable/main armhf Packages [75,0 kB]
    Réception de :6 https://packages.element.io/debian bullseye InRelease [2 902 B]
    Réception de :13 https://repo.charm.sh/apt * InRelease
    Err :6 https://packages.element.io/debian bullseye InRelease
      Les signatures suivantes ne sont pas valables : EXPKEYSIG C2850B265AC085BD riot.im packages <packages@riot.im>
    Atteint :15 http://apt.dalibo.org/labs bullseye-dalibo InRelease
    Atteint :16 https://apt.postgresql.org/pub/repos/apt bullseye-pgdg InRelease
    281 ko réceptionnés en 2s (134 ko/s)
    Lecture des listes de paquets... Fait
    W: Une erreur s'est produite lors du contrôle de la signature. Le dépôt n'est pas mis à jour et les fichiers d'index précédents seront utilisés. Erreur de GPG : https://packages.element.io/debian bullseye InRelease : Les signatures suivantes ne sont pas valables : EXPKEYSIG C2850B265AC085BD riot.im packages <packages@riot.im>
    W: Impossible de récupérer https://packages.riot.im/debian/dists/bullseye/InRelease  Les signatures suivantes ne sont pas valables : EXPKEYSIG C2850B265AC085BD riot.im packages <packages@riot.im>
    W: Le téléchargement de quelques fichiers d'index a échoué, ils ont été ignorés, ou les anciens ont été utilisés à la place.

    You can now start installing packages from apt.postgresql.org.

    Have a look at https://wiki.postgresql.org/wiki/Apt for more information;
    most notably the FAQ at https://wiki.postgresql.org/wiki/Apt/FAQ


Step 3 Add the TimescaleDB third party repository
----------------------------------------------------------

::

    echo "deb https://packagecloud.io/timescale/timescaledb/debian/ $(lsb_release -c -s) main" | sudo tee /etc/apt/sources.list.d/timescaledb.list

::

    deb https://packagecloud.io/timescale/timescaledb/debian/ bullseye main


Step 4 Install TimescaleDB GPG key
------------------------------------------

::

    wget --quiet -O - https://packagecloud.io/timescale/timescaledb/gpgkey | sudo apt-key add -

::

    Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).
    OK


Step 5 Update your local repository list
--------------------------------------------

::

    sudo apt update


Step 6 Install TimescaleDB
-----------------------------


::

    sudo apt install timescaledb-2-postgresql-15

::

    Lecture des listes de paquets... Fait
    Construction de l'arbre des dépendances... Fait
    Lecture des informations d'état... Fait
    Les paquets supplémentaires suivants seront installés :
      timescaledb-2-loader-postgresql-15 timescaledb-toolkit-postgresql-15 timescaledb-tools
    Les NOUVEAUX paquets suivants seront installés :
      timescaledb-2-loader-postgresql-15 timescaledb-2-postgresql-15 timescaledb-toolkit-postgresql-15 timescaledb-tools
    0 mis à jour, 4 nouvellement installés, 0 à enlever et 1 non mis à jour.
    Il est nécessaire de prendre 18,6 Mo dans les archives.
    Après cette opération, 155 Mo d'espace disque supplémentaires seront utilisés.
    Souhaitez-vous continuer ? [O/n] O
    Réception de :1 https://packagecloud.io/timescale/timescaledb/debian bullseye/main amd64 timescaledb-2-loader-postgresql-15 amd64 2.11.1~debian11 [90,4 kB]
    Réception de :2 https://packagecloud.io/timescale/timescaledb/debian bullseye/main amd64 timescaledb-2-postgresql-15 amd64 2.11.1~debian11 [14,0 MB]
    Réception de :3 https://packagecloud.io/timescale/timescaledb/debian bullseye/main amd64 timescaledb-tools amd64 0.14.3~debian11 [2 112 kB]
    Réception de :4 https://packagecloud.io/timescale/timescaledb/debian bullseye/main amd64 timescaledb-toolkit-postgresql-15 amd64 1:1.17.0~debian11 [2 462 kB]
    18,6 Mo réceptionnés en 3s (5 998 ko/s)
    Sélection du paquet timescaledb-2-loader-postgresql-15 précédemment désélectionné.
    (Lecture de la base de données... 585265 fichiers et répertoires déjà installés.)
    Préparation du dépaquetage de .../timescaledb-2-loader-postgresql-15_2.11.1~debian11_amd64.deb ...
    Dépaquetage de timescaledb-2-loader-postgresql-15 (2.11.1~debian11) ...
    Sélection du paquet timescaledb-2-postgresql-15 précédemment désélectionné.
    Préparation du dépaquetage de .../timescaledb-2-postgresql-15_2.11.1~debian11_amd64.deb ...
    Dépaquetage de timescaledb-2-postgresql-15 (2.11.1~debian11) ...
    Sélection du paquet timescaledb-tools précédemment désélectionné.
    Préparation du dépaquetage de .../timescaledb-tools_0.14.3~debian11_amd64.deb ...
    Dépaquetage de timescaledb-tools (0.14.3~debian11) ...
    Sélection du paquet timescaledb-toolkit-postgresql-15 précédemment désélectionné.
    Préparation du dépaquetage de .../timescaledb-toolkit-postgresql-15_1%3a1.17.0~debian11_amd64.deb ...
    Dépaquetage de timescaledb-toolkit-postgresql-15 (1:1.17.0~debian11) ...
    Paramétrage de timescaledb-tools (0.14.3~debian11) ...
    Paramétrage de timescaledb-2-loader-postgresql-15 (2.11.1~debian11) ...
    Paramétrage de timescaledb-toolkit-postgresql-15 (1:1.17.0~debian11) ...
    Paramétrage de timescaledb-2-postgresql-15 (2.11.1~debian11) ...
    RECOMMENDED: Run 'timescaledb-tune' (installed as part of the
    timescaledb-tools package, a recommended dependency) to update your
    config settings for TimescaleDB:

      timescaledb-tune --quiet --yes

    IF NOT, you need to update your postgresql.conf file to load TimescaleDB
    by adding 'timescaledb' to your shared_preload_libraries.
    Find the line below and change the value as shown (uncomment if needed):

    shared_preload_libraries = 'timescaledb'
    Traitement des actions différées (« triggers ») pour postgresql-common (250.pgdg110+1) ...
    Building PostgreSQL dictionaries from installed myspell/hunspell packages...
      en_us
      fr
    Removing obsolete dictionary files:


Step 7 Configure your database by running the timescaledb-tune script
-----------------------------------------------------------------------------

::

    sudo timescaledb-tune --quiet --yes


::

    Using postgresql.conf at this path:
    /etc/postgresql/15/main/postgresql.conf

    Writing backup to:
    /tmp/timescaledb_tune.backup202307251625

    Recommendations based on 15.40 GB of available memory and 12 CPUs for PostgreSQL 15
    shared_preload_libraries = 'timescaledb'	# (change requires restart)
    shared_buffers = 3942MB
    effective_cache_size = 11828MB
    maintenance_work_mem = 1971MB
    work_mem = 3364kB
    timescaledb.max_background_workers = 16
    max_worker_processes = 31
    max_parallel_workers_per_gather = 6
    max_parallel_workers = 12
    wal_buffers = 16MB
    min_wal_size = 512MB
    default_statistics_target = 500
    random_page_cost = 1.1
    checkpoint_completion_target = 0.9
    max_locks_per_transaction = 128
    autovacuum_max_workers = 10
    autovacuum_naptime = 10
    effective_io_concurrency = 256
    timescaledb.last_tuned = '2023-07-25T16:25:25+02:00'
    timescaledb.last_tuned_version = '0.14.3'
    Saving changes to: /etc/postgresql/15/main/postgresql.conf


Set up the TimescaleDB extension
=====================================

- https://docs.timescale.com/self-hosted/latest/install/installation-linux/#set-up-the-timescaledb-extension

When you have PostgreSQL and TimescaleDB installed, you can connect to it
from your local system using the psql command-line utility.


Install psql on Linux
-----------------------


Make sure your apt repository is up to date:
++++++++++++++++++++++++++++++++++++++++++++++++++

::

    sudo apt-get update



Install the postgresql-client package:
++++++++++++++++++++++++++++++++++++++++++++++++++++++

::

    sudo apt-get install postgresql-client


::

    Lecture des listes de paquets... Fait
    Construction de l'arbre des dépendances... Fait
    Lecture des informations d'état... Fait
    Les NOUVEAUX paquets suivants seront installés :
    postgresql-client
    0 mis à jour, 1 nouvellement installés, 0 à enlever et 1 non mis à jour.
    Il est nécessaire de prendre 68,2 ko dans les archives.
    Après cette opération, 72,7 ko d'espace disque supplémentaires seront utilisés.
    Réception de :1 https://apt.postgresql.org/pub/repos/apt bullseye-pgdg/main amd64 postgresql-client all 15+250.pgdg110+1 [68,2 kB]
    68,2 ko réceptionnés en 1s (72,2 ko/s)
    Sélection du paquet postgresql-client précédemment désélectionné.
    (Lecture de la base de données... 585368 fichiers et répertoires déjà installés.)
    Préparation du dépaquetage de .../postgresql-client_15+250.pgdg110+1_all.deb ...
    Dépaquetage de postgresql-client (15+250.pgdg110+1) ...
    Paramétrage de postgresql-client (15+250.pgdg110+1) ...


Setting up the TimescaleDB extension on Debian-based systems
==================================================================

1. Restart PostgreSQL and create the TimescaleDB extension:
--------------------------------------------------------------

::

    sudo systemctl restart postgresql

2. On your local system, at the command prompt, open the psql command-line utility as the postgres superuser
---------------------------------------------------------------------------------------------------------------

::


    sudo -u postgres psql --port 5433


::

    psql (15.3 (Debian 15.3-1.pgdg110+1))
    Saisissez « help » pour l'aide.

    postgres=#


3. Set the password for the postgres user
------------------------------------------------

::

    ✦ ❯ sudo -u postgres psql --port 5433

::

    psql (15.3 (Debian 15.3-1.pgdg110+1))
    Saisissez « help » pour l'aide.

    postgres=# \password postgres
    Saisir le nouveau mot de passe de l'utilisateur « postgres » :
    Saisir le mot de passe à nouveau :
    postgres=# exit

4. Exit from PostgreSQL
----------------------------

::


    \q


5. Use psql client to connect to PostgreSQL
--------------------------------------------------

::

    psql -U postgres -h localhost --port 5433 --password

::

    Mot de passe :
    psql (15.3 (Debian 15.3-1.pgdg110+1))
    Connexion SSL (protocole : TLSv1.3, chiffrement : TLS_AES_256_GCM_SHA384, compression : désactivé)
    Saisissez « help » pour l'aide.


6. At the psql prompt, create an empty database. Our database is called tsdb
------------------------------------------------------------------------------

::

    db_intranet=# CREATE database tsdb;

::


    CREATE DATABASE

7. Connect to the database you created
-------------------------------------------


    db_intranet=# \c tsdb


    Mot de passe :
    Connexion SSL (protocole : TLSv1.3, chiffrement : TLS_AES_256_GCM_SHA384, compression : désactivé)
    Vous êtes maintenant connecté à la base de données « tsdb » en tant qu'utilisateur « postgres ».
    tsdb=#


8. Add the TimescaleDB extension
-------------------------------------

1. Getting started: https://docs.timescale.com/timescaledb/latest/getting-started
2. API reference documentation: https://docs.timescale.com/api/latest
3. How TimescaleDB is designed: https://docs.timescale.com/timescaledb/latest/overview/core-concepts

Note: TimescaleDB collects anonymous reports to better understand and assist our users.
For more information and how to disable, please see our docs https://docs.timescale.com/timescaledb/latest/how-to-guides/configuration/telemetry.


::

    CREATE EXTENSION IF NOT EXISTS timescaledb


::

    ATTENTION:
    WELCOME TO
     _____ _                               _     ____________
    |_   _(_)                             | |    |  _  \ ___ \
      | |  _ _ __ ___   ___  ___  ___ __ _| | ___| | | | |_/ /
      | | | |  _ ` _ \ / _ \/ __|/ __/ _` | |/ _ \ | | | ___ \
      | | | | | | | | |  __/\__ \ (_| (_| | |  __/ |/ /| |_/ /
      |_| |_|_| |_| |_|\___||___/\___\__,_|_|\___|___/ \____/
                   Running version 2.11.1
    For more information on TimescaleDB, please visit the following links:

     1. Getting started: https://docs.timescale.com/timescaledb/latest/getting-started
     2. API reference documentation: https://docs.timescale.com/api/latest
     3. How TimescaleDB is designed: https://docs.timescale.com/timescaledb/latest/overview/core-concepts

    Note: TimescaleDB collects anonymous reports to better understand and assist our users.
    For more information and how to disable, please see our docs https://docs.timescale.com/timescaledb/latest/how-to-guides/configuration/telemetry.

    CREATE EXTENSION


9. On constate la création de nombreuses tables timescaleDB
----------------------------------------------------------------

.. figure:: images/tables_timescaledb.png
   :align: center


Comparaison avec l'installation des tables locust-ci installées dans la base de données 'postgres'
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. figure:: images/locust_tables_timescaledb.png
   :align: center


Disabling telemetry
======================

- https://docs.timescale.com/self-hosted/latest/configuration/telemetry/#disabling-telemetry
- https://docs.timescale.com/self-hosted/latest/configuration/postgres-config/

1. Open your PostgreSQL configuration file, and locate the timescaledb.telemetry_level parameter
--------------------------------------------------------------------------------------------------

See the PostgreSQL configuration file instructions for locating and
opening the file.

::

    psql -U postgres -h localhost --port 5433 --password

::

    ✦ ❯ psql -U postgres -h localhost --port 5433 --password

::


    Mot de passe :
    psql (15.3 (Debian 15.3-1.pgdg110+1))
    Connexion SSL (protocole : TLSv1.3, chiffrement : TLS_AES_256_GCM_SHA384, compression : désactivé)
    Saisissez « help » pour l'aide.

    postgres=# SHOW config_file;
                   config_file
    -----------------------------------------
     /etc/postgresql/15/main/postgresql.conf
    (1 ligne)

    postgres=#


2. Add the parameter setting to off
----------------------------------------------

::

    timescaledb.telemetry_level=off


3. Reload the configuration file
------------------------------------


::

    sudo service postgresql restart

