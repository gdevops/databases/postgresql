.. index::
   pair: PostgreSQL ; Extensions
   pair: Extensions ; RSS (https://botsin.space/@pgxn.rss)
   ! Extensions


.. _pg_extensions:

========================================================
**PostgreSQL extensions**
========================================================

- https://github.com/pgxn/pgxn-api/wiki
- https://botsin.space/@pgxn
- https://botsin.space/@pgxn.rss
- https://www.postgresql.org/docs/current/contrib.html
- https://wiki.postgresql.org/wiki/Extensions

.. toctree::
   :maxdepth: 3

   definition/definition
   installation/installation
   pg_crypto/pg_crypto
   postgresql_anonymizer/postgresql_anonymizer
   timescaledb/timescaledb
