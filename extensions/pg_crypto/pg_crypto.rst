.. index::
   pair: PostgreSQL extension; pgcrypto
   ! pgcrypto


.. _pgcrypto:

====================================================================
**pgcrypto** (fonctions de cryptographie pour PostgreSQL)
====================================================================

- https://docs.postgresql.fr/15/pgcrypto.html


Description
============

Le module pgcrypto propose des fonctions de cryptographie pour PostgreSQL.

Ce module est considéré comme « trusted », ce qui signifie qu'il peut
être installé par des utilisateurs simples (sans attribut SUPERUSER) et
qui ont l'attribut CREATE sur la base de données courante.

.. warning:: pgcrypto requiert OpenSSL et ne sera pas installé si le
   support d'OpenSSL n'a pas été sélectionné lors de la construction
   de PostgreSQL.


Prerequisite
===============

- :ref:`pg_extensions_install`


Install the postgresql-contrib package.


Installation
============


::

    CREATE EXTENSION IF NOT EXISTS pgcrypto;



\dx+
=======

::

    postgres=# \dx+

::


             Objets dans l'extension « pgcrypto »
                      Description d'objet
    -------------------------------------------------------
     fonction armor(bytea)
     fonction armor(bytea,text[],text[])
     fonction crypt(text,text)
     fonction dearmor(text)
     fonction decrypt(bytea,bytea,text)
     fonction decrypt_iv(bytea,bytea,bytea,text)
     fonction digest(bytea,text)
     fonction digest(text,text)
     fonction encrypt(bytea,bytea,text)
     fonction encrypt_iv(bytea,bytea,bytea,text)
     fonction gen_random_bytes(integer)
     fonction gen_random_uuid()
     fonction gen_salt(text)
     fonction gen_salt(text,integer)
     fonction hmac(bytea,bytea,text)
     fonction hmac(text,text,text)
     fonction pgp_armor_headers(text)
     fonction pgp_key_id(bytea)
     fonction pgp_pub_decrypt(bytea,bytea)
     fonction pgp_pub_decrypt_bytea(bytea,bytea)
     fonction pgp_pub_decrypt_bytea(bytea,bytea,text)
     fonction pgp_pub_decrypt_bytea(bytea,bytea,text,text)
     fonction pgp_pub_decrypt(bytea,bytea,text)
     fonction pgp_pub_decrypt(bytea,bytea,text,text)
     fonction pgp_pub_encrypt_bytea(bytea,bytea)
     fonction pgp_pub_encrypt_bytea(bytea,bytea,text)
     fonction pgp_pub_encrypt(text,bytea)
     fonction pgp_pub_encrypt(text,bytea,text)
     fonction pgp_sym_decrypt_bytea(bytea,text)
     fonction pgp_sym_decrypt_bytea(bytea,text,text)
     fonction pgp_sym_decrypt(bytea,text)
     fonction pgp_sym_decrypt(bytea,text,text)
     fonction pgp_sym_encrypt_bytea(bytea,text)
     fonction pgp_sym_encrypt_bytea(bytea,text,text)
     fonction pgp_sym_encrypt(text,text)
     fonction pgp_sym_encrypt(text,text,text)
    (36 lignes)
