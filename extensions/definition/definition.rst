.. index::
   pair: PostgreSQL extensions; Definition


.. _pg_extensions_def:

========================================================
PostgreSQL extensions definition
========================================================

.. seealso::

   - https://wiki.postgresql.org/wiki/Extensions

Extensions were implemented in PostgreSQL 9.1 to allow for easier packaging of
additions to PostgreSQL.

Extensions can package user-visible functions or use hooks in the PostgreSQL to
modify how the database does certain processes.
