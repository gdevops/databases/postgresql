
.. _postgresql_1_1_0:

=======================================================================================
**PostgreSQL Anonymizer 1.1.0** (2022-09-28)
=======================================================================================

- https://gitlab.com/dalibo/postgresql_anonymizer/-/tags/1.1.0
