.. index::
   pair: PostgreSQL extension; postgresql_anonymizer
   ! postgresql_anonymizer


.. _postgresql_anonymizer:

=======================================================================================
|ano| **PostgreSQL Anonymizer**
=======================================================================================

🐘🕶️ Anonymization & Data Masking for PostgreSQL

- https://gitlab.com/dalibo/postgresql_anonymizer
- https://labs.dalibo.com/postgresql_anonymizer

.. toctree::
   :maxdepth: 3

   description/description
   articles/articles
   versions/versions


