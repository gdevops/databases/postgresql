

========================================================================
2022-05
========================================================================


2022-05-19 **PostgreSQL et le principe de "Privacy By Design"**
=================================================================

- https://postgresql-anonymizer.readthedocs.io/en/latest/
- https://blog.dalibo.com/2022/05/23/privacy-by-design.html
- https://cnpd.public.lu/fr/dossiers-thematiques/nouvelles-tech-communication/privacy-by-design.html

La `version 1.0 <https://www.postgresql.org/about/news/postgresql-anonymizer-10-privacy-by-design-for-postgres-2452/>`_
de l’extension PostgreSQL Anonymizer a été publiée il y a quelques jours.

L’occasion de revenir sur le concept de “Privacy By Design“ qui est une
des obligations du RGPD.

**Quatre ans après la mise en route du RGPD, son application reste complexe
pour de nombreuses entreprises et organisations**.

En particulier, la mise en oeuvre du principe “privacy by design” reste
un casse-tête…

En effet, l’article 25 du RGPD impose la “Protection des données dès la
conception” en précisant que la protection des droits et libertés des
personnes physiques doit être mise en oeuvre « tant au moment de la
détermination des moyens du traitement qu’au moment du traitement lui-même.»

Mais comment intégrer des règles de protection des données dès la
conception d’une application ?

La majorité des outils d’anonymisation actuels fonctionnent à l’extérieur
de la base de données, sur le principe des outils ETL (Extract-Transform-Load).
Il en résulte que la responsabilité de la rédaction de la politique de
sécurité est généralement confiée aux DBA de production.
En clair, ces outils se focalisent sur la phase de traitement des données
plutôt que sur la phase de détermination de ces traitements.

logo de PostgreSQL Anonymizer

À l’inverse, l’extension PostgreSQL Anonymizer cherche à impliquer les
développeurs et les architectes dès les phases préliminaires de conception.

Grace à une syntaxe SQL appelées “SECURITY LABEL”, les règles de masquage
sont déclarées directement à l’intérieur même de la base, au même titre
qu’une contrainte d’intégrité ou d’un index.

