
.. _anonymizer_desc:

=======================================================================================
Description
=======================================================================================

Description
------------

**postgresql_anonymizer** is an extension to mask or replace personally
identifiable information (PII) or commercially sensitive data from a
PostgreSQL database.

The project is aiming toward a **declarative approach of anonymization**.

This means we’re trying to **extend PostgreSQL Data Definition Language (DDL)**
in order to specify the anonymization strategy inside the table definition
itself.

Success Stories
-----------------

.. admonition:: Thierry Aimé, Office of Architecture and Standards in the French Public Finances Directorate General (DGFiP)

    With PostgreSQL Anonymizer we integrate, from the design of the database,
    the principle that outside production the data must be anonymized.

    Thus we can reinforce the GDPR rules, without affecting the quality
    of the tests during version upgrades for example.


.. admonition:: Grégory GNOS, IT Solution MW at bioMérieux

    The PostgreSQL Anonymizer extension immediately aroused our interest
    at bioMérieux.

    This innovative extension allowed us to integrate the anonymization
    of patient data at the earliest stage of the development process.

    Therefore we could shorten our implementation times to be more
    responsive to our customers.


