.. index::
   pair: PostgreSQL ; extensions


.. _pg_extensions_install:

========================================================
PostgreSQL extensions installation
========================================================


.. figure:: postgresql_extensions.JPG
   :align: center

   "Mastering PostgreSQL in Application Development" Dimitri Fontaine (https://x.com/tapoueh), 2017, p. 198


On Debian based
==================

::

    sudo apt install postgresql-contrib


SELECT * FROM pg_available_extensions;
==========================================

::

    postgres=# SELECT * FROM pg_available_extensions;

::


            name        | default_version | installed_version |                               comment
    --------------------+-----------------+-------------------+----------------------------------------------------------------------
     citext             | 1.5             |                   | data type for case-insensitive character strings
     autoinc            | 1.0             |                   | functions for autoincrementing fields
     pageinspect        | 1.7             |                   | inspect the contents of database pages at a low level
     pgstattuple        | 1.5             |                   | show tuple-level statistics
     earthdistance      | 1.1             |                   | calculate great-circle distances on the surface of the Earth
     pgrowlocks         | 1.2             |                   | show row-level locking information
     insert_username    | 1.0             |                   | functions for tracking who changed a table
     xml2               | 1.1             |                   | XPath querying and XSLT
     seg                | 1.3             |                   | data type for representing line segments or floating-point intervals
     tcn                | 1.0             |                   | Triggered change notifications
     plpgsql            | 1.0             | 1.0               | PL/pgSQL procedural language
     btree_gist         | 1.5             |                   | support for indexing common datatypes in GiST
     ltree              | 1.1             |                   | data type for hierarchical tree-like structures
     pg_prewarm         | 1.2             |                   | prewarm relation data
     postgres_fdw       | 1.0             |                   | foreign-data wrapper for remote PostgreSQL servers
     pg_visibility      | 1.2             |                   | examine the visibility map (VM) and page-level visibility info
     dblink             | 1.2             |                   | connect to other PostgreSQL databases from within a database
     isn                | 1.2             |                   | data types for international product numbering standards
     btree_gin          | 1.3             |                   | support for indexing common datatypes in GIN
     sslinfo            | 1.2             |                   | information about SSL certificates
     pg_freespacemap    | 1.2             |                   | examine the free space map (FSM)
     intagg             | 1.1             |                   | integer aggregator and enumerator (obsolete)
     tsm_system_time    | 1.0             |                   | TABLESAMPLE method which accepts time in milliseconds as a limit
     fuzzystrmatch      | 1.1             |                   | determine similarities and distance between strings
     tablefunc          | 1.0             |                   | functions that manipulate whole tables, including crosstab
     cube               | 1.4             |                   | data type for multidimensional cubes
     pg_trgm            | 1.4             |                   | text similarity measurement and index searching based on trigrams
     pg_stat_statements | 1.6             |                   | track execution statistics of all SQL statements executed
     intarray           | 1.2             |                   | functions, operators, and index support for 1-D arrays of integers
     lo                 | 1.1             |                   | Large Object maintenance
     amcheck            | 1.1             |                   | functions for verifying relation integrity
     dict_int           | 1.0             |                   | text search dictionary template for integers
     adminpack          | 2.0             |                   | administrative functions for PostgreSQL
     tsm_system_rows    | 1.0             |                   | TABLESAMPLE method which accepts number of rows as a limit
     dict_xsyn          | 1.0             |                   | text search dictionary template for extended synonym processing
     refint             | 1.0             |                   | functions for implementing referential integrity (obsolete)
     pgcrypto           | 1.3             | 1.3               | cryptographic functions
     bloom              | 1.0             |                   | bloom access method - signature file based index
     file_fdw           | 1.0             |                   | foreign-data wrapper for flat file access
     moddatetime        | 1.0             |                   | functions for tracking last modification time
     hstore             | 1.5             |                   | data type for storing sets of (key, value) pairs
     timetravel         | 1.0             |                   | functions for implementing time travel
     pg_buffercache     | 1.3             |                   | examine the shared buffer cache
     unaccent           | 1.1             |                   | text search dictionary that removes accents
     uuid-ossp          | 1.1             |                   | generate universally unique identifiers (UUIDs)
    (45 lignes)
