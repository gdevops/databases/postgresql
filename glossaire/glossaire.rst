.. index::
   pair: PostgreSQL ; Glossaire
   ! Glossaire PostgreSQL

.. _postgresql_glossaire:

========================================================
**Glossaire** PostgreSQL
========================================================

TODO
-----

- https://www.crunchydata.com/blog/challenging-postgres-terminology


:source:
    - :ref:`chers_lectrices_dalibo`
    - :ref:`dalibo_dev_tuto_sql_advanded`


.. glossary::

    base
    base de données
        Une **base de données** est un conteneur hermétique.
        En dehors des objets globaux, le rôle connecté à une base de données
        ne voit et ne peut interagir qu’avec les objets contenus dans cette base.

        De même, il ne voit pas les objets locaux des autres bases.
        Néanmoins, il est possible de lui donner le droit d’accéder à
        certains objets d’une autre base (de la même instance ou d’une
        autre instance) en utilisant les **Foreign Data Wrappers** (postgres_fdw)
        ou l’extension dblink.

        **Un rôle ne se connecte pas à l’instance**. Il se connecte forcément
        à une **base spécifique**.

        Source: :ref:`dalibo_dev_tuto_sql_advanded`

    Hypertable
    Hypertables
        Hypertables are PostgreSQL tables with special features that make
        it easy to handle time-series data.

        Anything you can do with regular PostgreSQL tables, you can do
        with hypertables. In addition, you get the benefits of improved
        performance and user experience for **time-series data**.

        Source:

        - https://docs.timescale.com/use-timescale/latest/hypertables/

        Hypertables are PostgreSQL tables that automatically partition
        your data by time.
        You interact with hypertables in the same way as regular PostgreSQL
        tables, but with extra features that makes managing your time-series
        data much easier.

        In Timescale, hypertables exist alongside regular PostgreSQL tables.
        Use hypertables to store time-series data.
        This gives you improved insert and query performance, and access
        to useful time-series features. Use regular PostgreSQL tables
        for other relational data.

        With hypertables, Timescale makes it easy to improve insert and
        query performance by partitioning time-series data on its time
        parameter.
        Behind the scenes, the database performs the work of setting up
        and maintaining the hypertable's partitions.

        Meanwhile, you insert and query your data as if it all lives in
        a single, regular PostgreSQL table.

        Source:

        - https://docs.timescale.com/use-timescale/latest/hypertables/about-hypertables/


    instance
        Une **instance** est un **ensemble de bases de données, de rôles et de tablespaces**.

        Ces objets sont appelés des objets globaux parce qu’ils sont disponibles
        quelque soit la base de données de connexion.
        Chaque base de données contient ensuite des objets qui lui sont propres.

        Ils sont spécifiques à cette base de données et accessibles uniquement
        lorsque l’utilisateur est connecté à la base qui les contient.
        Il est donc possible de voir les bases comme des conteneurs hermétiques
        en dehors des objets globaux

        Source: :ref:`dalibo_dev_tuto_sql_advanded`


    modèle relationnel
        Le **modèle relationnel** est apparu suite à un constat : les bases
        de données de l’époque (hiérarchiques) reposaient sur la notion
        de pointeur.

        Une mise à jour pouvait donc facilement casser le modèle : doublons simples,
        données pointant sur du « vide », doublons incohérents entre eux, etc.

        Le **modèle relationnel** a donc été proposé pour remédier à tous ces
        problèmes. Un système relationnel repose sur le concept de relation
        (table en SQL).
        Une relation est un ensemble de faits. Chaque fait est identifié
        par un identifiant (clé naturelle).
        Le fait lie cet identifiant à un certain nombre d’attributs.
        Une relation ne peut donc pas avoir de doublon.
        La modélisation relationnelle étant un vaste sujet en soi, nous
        n’allons pas tout détailler ici, mais plutôt rappeler les points
        les plus importants.

        Source: :ref:`dalibo_dev_tuto_sql_advanded`

    PL
        **PL** est l’acronyme de **Procedural Languages**.

        En dehors du C et du SQL, tous les langages acceptés par PostgreSQL sont des **PL**.

        Par défaut, trois langages sont installés et activés : C, SQL et PL/pgSQL.

        Les quatre langages PL supportés nativement sont décrits en détail
        dans la documentation officielle:

        • `PL/PgSQL <https://docs.postgresql.fr/current/plpgsql.html>`_ (https://docs.postgresql.fr/current/plpgsql.html)
        • PL/Tcl (existe en version trusted et untrusted)
        • PL/Perl (existe en version trusted et untrusted)
        • `PL/Python <https://docs.postgresql.fr/current/plpython.html>`_ (https://docs.postgresql.fr/current/plpython.html) (uniquement en version untrusted

        Source: :ref:`dalibo_dev_tuto_sql_advanded`


    relationnel
        Contrairement à une idée assez fréquemment répandue, le terme
        **relationnel** ne désigne pas le fait que les tables soient liées
        entre elles.
        Les **tables** SONT les relations. On fait référence ici à l’algèbre
        relationnelle, inventée en 1970 par `Edgar Frank Codd <https://fr.wikipedia.org/wiki/Edgar_Frank_Codd>`_ (https://fr.wikipedia.org/wiki/Edgar_Frank_Codd).

        Les bases de données dites relationnelles n’implémentent habituellement
        pas exactement cet algèbre, mais en sont très proches.

        Le langage SQL, entre autres, ne respecte pas l’algèbre relationnelle.
        Le sujet étant vaste et complexe, il ne sera pas abordé ici. Si vous
        voulez approfondir le sujet, le livre Introduction aux bases de données
        de Chris J. Date, est un des meilleurs ouvrages sur l’algèbre
        relationnelle et les déficiences du langage SQL à ce sujet.

        Source: :ref:`dalibo_dev_tuto_sql_advanded`


    rôle
        Une instance contient un ensemble de **rôles**.

        Certains sont prédéfinis et permettent de disposer de droits
        particuliers (lecture de fichier avec pg_read_server_files, annulation
        d’une requête avec pg_signal_backend, etc).
        Cependant, la majorité est composée de rôles créés pour permettre
        la connexion des utilisateurs

        Source: :ref:`dalibo_dev_tuto_sql_advanded`


    schéma

        **Definition 1 Dalibo**

        Les **schémas** sont des **espaces de noms** à l’intérieur d’une base de
        données permettant:

        • de grouper logiquement les objets d’une base de données ;
        • de séparer les utilisateurs entre eux ;
        • de contrôler plus efficacement les accès aux données ;
        • d’éviter les conflits de noms dans les grosses bases de données

        Un schéma n’a à priori aucun lien avec un utilisateur donné.

        Un schéma est un espace logique sans lien avec les emplacements
        physiques des données (ne pas confondre avec les tablespaces).

        Un utilisateur peut avoir accès à tous les schémas ou à un sous-ensemble,
        tout dépend des droits dont il dispose.

        Par défaut, avant la version PostgreSQL 15 il avait accès au
        schéma **public** de chaque base et pouvait y créer des objets.

        **Definition 3 Crunchydata**

        Autre définition en anglais, https://www.crunchydata.com/blog/challenging-postgres-terminology

        I have to include "schema" here because on the one hand it is
        very easy, Craig already covered it in his blog post: a schema
        is what you get when you CREATE SCHEMA! It's like a folder in
        your database.

        But actually! Internally you will find that Postgres refers to
        "schemas" as "namespaces". You can see a list of all the schemas
        in your database by running a query on ... pg_namespace!

        SELECT * FROM pg_namespace

        And that's not all.

        When I was first introduced to "schema" I was very confused,
        because to my mind a "schema" was a "collection of table definitions
        in a database".
        So an "application schema" might be all the tables in a database
        used to support a particular software application.

        So, a "schema" is (to SQL) a folder where you can put database
        objects (like tables, functions, etc) and might (in Postgres)
        also be called a "namespace", and is sometimes (to an app developer)
        the definition of one or more tables.


        **Definition 3 Cybertec**

        - https://www.cybertec-postgresql.com/en/what-is-a-schema-in-postgresql/

        Before you figure out how to use schemas, you need to know what
        the purpose of a schema is in the first place.

        To understand that, first take a look at how PostgreSQL is structured:

        - Instance
        - Database
        - Schema
        - Table
        - Row

        An “instance” is basically what you start when you deploy PostgreSQL.

        The next layer is a database. In reality this is what you connect
        to: in PostgreSQL a connection is always bound to a database inside
        an instance, which happens early on, right after user authentication.

        What is important is the next layer down, between databases and
        tables: Schemas.


        Basically, schemas are **a way to group tables together**.

        Let’s assume there’s a fairly large data structure: Having 500
        tables in one place is certainly harder to manage and understand
        than to have 10 buckets containing 50 tables each.

        It’s simply like organizing pictures: You wouldn’t put all of them
        into the same folder, but rather group them by year, location, etc.

        The same logic can be applied to tables.

        **psql (\dn pour avoir la liste des schémas d'une base PostgreSQL)**

        psql postgresql://postgres:postgres@localhost:5433/postgres

        \dn

        ::

                          Liste des schémas
                       Nom            |   Propriétaire
            --------------------------+-------------------
             _timescaledb_cache       | postgres
             _timescaledb_catalog     | postgres
             _timescaledb_config      | postgres
             _timescaledb_functions   | postgres
             _timescaledb_internal    | postgres
             postgres                 | postgres
             public                   | pg_database_owner
             timescaledb_experimental | postgres
             timescaledb_information  | postgres
            (9 lignes)


    table
        Par défaut, les **tables** sont permanentes, journalisées et **non partitionnées**.

        Il est possible de créer des tables temporaires (CREATE TEMPORARY TABLE).
        Celles-ci ne sont visibles que par la session qui les a créées
        et seront supprimées par défaut à la fin de cette session.

        Il est aussi possible de les supprimer automatiquement à la fin
        de la transaction qui les a créées.
        Il n’existe pas dans PostgreSQL de notion de table temporaire globale.
        Cependant, une `extension <https://github.com/darold/pgtt>`_ (https://github.com/darold/pgtt)
        existe pour combler leur absence.

        Pour des raisons de performance, il est possible de créer une table non journalisée (CREATE
        UNLOGGED TABLE).

        La définition de la table est journalisée mais pas son contenu.
        De ce fait, en cas de crash, il est impossible de dire si la table
        est corrompue ou non, et donc, au redémarrage du serveur, PostgreSQL
        vide la table de tout contenu.
        De plus, n’étant pas journalisée, la table n’est pas présente dans
        les sauvegardes PITR, ni repliquée vers d’éventuels serveurs secondaires.

        Enfin, depuis la version 10, il est possible de **partitionner** les
        tables suivant un certain type de partitionnement : par intervalle,
        par valeur ou par hachage.
        Avant la version 10, il est possible de se rabattre sur le partitionnement
        par héritage, moins pratique et moins performant.

        Source: :ref:`dalibo_dev_tuto_sql_advanded`


    vue
        Le but des **vues** est de masquer une complexité, qu’elle soit du
        côté de la structure de la base ou de l’organisation des accès.

        Dans le premier cas, elles permettent de fournir un accès qui ne
        change pas même si les structures des tables évoluent.

        Dans le second cas, elles permettent l’accès à seulement certaines
        colonnes ou certaines lignes.

        De plus, les vues étant exécutées avec les mêmes droits que
        l’utilisateur qui les a créées, **cela permet un changement temporaire
        des droits d’accès très appréciable dans certains cas**.

        Source: :ref:`dalibo_dev_tuto_sql_advanded`

    vue matérialisée
        PostgreSQL gère le support natif des **vues matérialisées** (CREATE MATERIALIZED VIEW
        nom_vue_mat AS SELECT ...).

        Les vues matérialisées sont des vues dont le contenu est figé
        sur disque, permettant de ne pas recalculer leur contenu à chaque
        appel.

        De plus, il est possible de les indexer pour accélérer leur consultation.
        Il faut cependant faire attention à ce que leur contenu reste
        synchrone avec le reste des données

        Source: :ref:`dalibo_dev_tuto_sql_advanded`
