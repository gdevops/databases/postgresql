.. index::
   pair: PostgreSQL Tutorial; stormatics

.. _pg_tuto_stormatics:

========================================================
**PostgreSQL stormatics tutorial**
========================================================

.. toctree::
   :maxdepth: 3

   postgresql-internals-part-1-understanding-database-cluster-database-and-tables
   postgresql-internals-part-2-understanding-page-structure
