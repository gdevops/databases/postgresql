
.. _chers_lectrices_dalibo:

=================================================
Chers lectrices & lecteurs des documents Dalibo
=================================================

- :ref:`licence_dalibo`

Chers lectrices & lecteurs,

Nos formations PostgreSQL sont issues de nombreuses années d’études, d’expérience
de terrain et de passion pour les logiciels libres.

Pour Dalibo, l’utilisation de PostgreSQL n’est pas une marque d’opportunisme
commercial, mais l’expression d’un engagement de longue date.
Le choix de l’Open Source est aussi le choix de l’implication dans la
communauté du logiciel.

Au-delà du contenu technique en lui-même, notre intention est de transmettre
les valeurs qui animent et unissent les développeurs de PostgreSQL depuis
toujours : partage, ouverture, transparence, créativité, dynamisme...

Le but premier de nos formations est de vous aider à mieux exploiter toute
la puissance de PostgreSQL mais nous espérons également qu’elles vous
inciteront à devenir un membre actif de la communauté en partageant
à votre tour le savoir-faire que vous aurez acquis avec nous.

Nous mettons un point d’honneur à maintenir nos manuels à jour, avec des
informations précises et des exemples détaillés.
Toutefois malgré nos efforts et nos multiples relectures, il est probable
que ce document contienne des oublis, des coquilles, des imprécisions ou
des erreurs.

Si vous constatez un souci, n’hésitez pas à le signaler via l’adresse
formation@dalibo.com !


Remerciements
==============


Ce manuel de formation est une aventure collective qui se transmet au
sein de notre société depuis des années. Nous remercions chaleureusement
ici toutes les personnes qui ont contribué directement ou indirectement
à cet ouvrage, notamment :

- `Jean-Paul Argudo <https://x.com/jpargudo>`_,
- Alexandre Anriot,
- `Carole Arnaud <https://x.com/pg_krol>`_,
- Alexandre Baron,
- David Bidoc,
- Sharon Bonan,
- Franck Boudehen,
- Arnaud Bruniquel,
- `Damien Clochard <https://x.com/daamien>`_,
- Christophe Courtois,
- Marc Cousin,
- Gilles Darold,
- Jehan-Guillaume de Rorthais,
- Ronan Dunklau,
- `Vik Fearing <https://www.enterprisedb.com/vik-fearing>`_, => https://x.com/PostgreSQL
- Stefan Fercot,
- Pierre Giraud,
- Nicolas Gollet,
- `Dimitri Fontaine <https://x.com/tapoueh>`_,
- `Florent Jardin <https://x.com/fljdin>`_ => https://fljd.in/
- `Virginie Jourdan <https://x.com/JourdanVirginie>`_,
- Luc Lamarle,
- Denis Laxalde,
- `Guillaume Lelarge <https://x.com/g_lelarge>`_,
- Benoit Lobréau,
- Jean-Louis Louër,
- Thibaut Madelaine,
- `Adrien Nayrat <https://x.com/Adrien_nayrat>`_,
- Alexandre Pereira,
- Flavie Perette,
- Robin Portigliatti,
- Thomas Reiss,
- Maël Rimbault,
- Julien Rouhaud,
- Stéphane Schildknecht,
- Julien Tachoires,
- Nicolas Thauvin,
- Christophe Truffier,
- Cédric Villemain,
- Thibaud Walkowiak,
- Frédéric Yhuel.

À propos de DALIBO : DALIBO est le spécialiste français de PostgreSQL.
Nous proposons du support, de la formation et du conseil depuis 2005.
Retrouvez toutes nos formations sur https://dalibo.com/formations
