.. index::
   pair: DEVPG ; Dalibo Dev


.. _dalibo_dev_tuto_sql_advanded:

===============================================
Développement, DEVPG - Développer avec SQL
===============================================

- https://www.dalibo.com/formation-developper-postgresql
- https://public.dalibo.com/exports/formation/manuels/formations/devpg/devpg.handout.html
- https://public.dalibo.com/exports/formation/manuels/formations/devpg/devpg.handout.pdf

Objectifs
===========

Destinée aux développeurs expérimentés, cette formation leur permettra
de tirer pleinement parti de PostgreSQL® en donnant les clés pour :

- Optimiser une application
- Savoir lire un plan d’exécution
- Maîtriser l’indexation
- Écrire et maintenir des fonctions PL/pgSQL



Table des matières
====================

- https://public.dalibo.com/exports/formation/manuels/formations/devpg/devpg.handout.pdf


PostgreSQL: historique et communauté, Découverte des fonctionnalités, Introduction aux plans d'exécution
----------------------------------------------------------------------------------------------------------

.. figure:: images/tdm_1.png
   :align: center


PostgreSQL: Optimisations SQL
--------------------------------------------------------

.. figure:: images/tdm_2.png
   :align: center


Techniques d'indexation, Comprendre EXPLAIN
----------------------------------------------

.. figure:: images/tdm_3.png
   :align: center


SQL: Ce qu'il ne faut pas faire
---------------------------------------

.. figure:: images/tdm_4.png
   :align: center


PL/pgSQL: les bases
---------------------------------------

.. figure:: images/tdm_5.png
   :align: center


Thèmes abordés
=================

- Découvrir PostgreSQL
- Optimisations SQL
- Lire un plan d’exécution
- Programmation côté serveur


Durée
=======

La formation se déroule sur 3 jours (21 heures), uniquement en présentiel.


Public concerné
=================

Cette formation s’adresse aux développeurs ayant une bonne expérience
du langage SQL et qui souhaitent aller plus loin dans leur maîtrise de
ce langage et des spécificités de PostgreSQL.

Pré-requis
==============

Avant la formation, nous demandons aux stagiaires de passer un test
QCM afin de vérifier les pré-requis suivants :

- Pratique du langage SQL
