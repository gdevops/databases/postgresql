.. index::
   pair: DEVSQLPG ; Dalibo Dev


.. _dalibo_dev_tuto_sqlpg_advanded:

===============================================
Développement, DEVSQLPG - SQL pour PostgreSQL
===============================================

- :ref:`chers_lectrices_dalibo`
- :ref:`licence_dalibo`
- https://www.dalibo.com/formation-sql-pour-postgresql
- https://public.dalibo.com/exports/formation/manuels/formations/devsqlpg/devsqlpg.handout.pdf

Objectifs
===========

Cette formation s’adresse aux développeurs ayant besoin d’améliorer
leurs connaissances et leurs compétences en **langage SQL**.

Ils pourront revoir les bases et aller plus loin dans leur maîtrise de
ce langage et des spécificités de PostgreSQL :

- Écrire et maintenir des requêtes SQL
- Maîtriser les types avancés
- Comprendre le principe de transaction
- Maîtriser les fonctions window et les CTE
- Écrire et maintenir des fonctions PL/pgSQL
- Connaître les différents connecteurs



Table des matières
====================

- https://public.dalibo.com/exports/formation/manuels/formations/devsqlpg/devsqlpg.handout.pdf


PostgreSQL: historique et communauté, Découverte des fonctionnalités, Introduction et premiers SELECT, Création d'objets et mises à jour
------------------------------------------------------------------------------------------------------------------------------------------

.. figure:: images/tdm_1.png
   :align: center


Plus loin avec SQL, SQL avancé pour le transactionnel
--------------------------------------------------------

.. figure:: images/tdm_2.png
   :align: center


Types de base, Types avancés, SQL pour l'analyse de données
-------------------------------------------------------------

.. figure:: images/tdm_3.png
   :align: center


Ce qu'il ne faut pas faire
---------------------------------------

.. figure:: images/tdm_4.png
   :align: center


PL/pgSQL: les bases, PL/pgSQL avancé
---------------------------------------

.. figure:: images/tdm_5.png
   :align: center

Extensions PostgreSQL pour l'utilisateur, Partitionnement
-----------------------------------------------------------

.. figure:: images/tdm_6.png
   :align: center

Connexions distantes, Fonctionnalités avancées pour la performance, Pooling
-----------------------------------------------------------------------------

.. figure:: images/tdm_7.png
   :align: center


Thèmes abordés
===============

- Découvrir PostgreSQL
- Bases du SQL
- Types de données avancés
- Richesses de l’écosystème PostgreSQL
- Étendre PostgreSQL
- Programmation côté serveur

Durée
=========

La formation se déroule sur 3 jours (21 heures), uniquement en intra-entreprise
et en présentiel.
