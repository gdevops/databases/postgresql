.. index::
   pair: PostgreSQL ; Dalibo Perf1


.. _dalibo_perf1_tuto:

==================================
PERF1 PostgreSQL Performances
==================================

- :ref:`chers_lectrices_dalibo`
- :ref:`licence_dalibo`
- https://www.dalibo.com/formation-postgresql-performance-1
- https://public.dalibo.com/exports/formation/manuels/formations/perf1/perf1.handout.pdf

Objectifs
==========

Cette formation montre comment optimiser avec efficacité un serveur
PostgreSQL.
Elle permet d’améliorer les performances du système avec différents
leviers :

- Virtualiser sans renoncer aux performances
- Exploiter au mieux le matériel
- Adapter le système d’exploitation
- Ajuster les paramètres de configuration
- Optimiser la consommation mémoire
- Détecter les requêtes les plus lentes
- Créer un index
- Comprendre un plan d’exécution



Table des matières
====================

- https://public.dalibo.com/exports/formation/manuels/formations/perf1/perf1.handout.pdf


Configuration du système et de l'instance, Introduction aux plans d'exécution, Techniques d'indexation
---------------------------------------------------------------------------------------------------------------------

.. figure:: images/tdm_1.png
   :align: center


Comprendre EXPLAIN
---------------------------------------

.. figure:: images/tdm_2.png
   :align: center


Référence sur les noeuds d'exécution, Analyses et diagnostics
------------------------------------------------------------------


.. figure:: images/tdm_3.png
   :align: center



Durée
======

La formation se déroule sur 3 jours (21 heures) en présentiel, ou
5 matinées (17,5 heures) à distance.

Public concerné
=================

Ce programme s’adresse aux utilisateurs confirmés et aux administrateurs
de bases de données (DBA) ayant déjà une bonne connaissance de PostgreSQL
ou d’un autre SGBD du marché.

Pré-requis
==============

Avant la formation, nous demandons aux stagiaires de passer un test
QCM afin de vérifier les pré-requis suivants :

- Notions de base en langage SQL
- Bonne connaissance de PostgreSQL

La formation :ref:`Dalibo DBA1 <dalibo_admin_dba1>` est conseillée.
