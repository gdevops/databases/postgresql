.. index::
   pair: PostgreSQL ; Dalibo Performance


.. _dalibo_perf_tuto:

========================================================
Dalibo Performance PostgreSQL tutorials
========================================================

- https://www.dalibo.com/formations
- https://www.dalibo.com/formation-administration


.. toctree::
   :maxdepth: 3

   perf1/perf1
   perf2/perf2
