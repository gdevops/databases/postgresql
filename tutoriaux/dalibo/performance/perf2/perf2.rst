.. index::
   pair: PostgreSQL ; Dalibo Perf2

.. _dalibo_perf2_tuto:

==================================
PERF2 Indexation et SQL avancés
==================================

- :ref:`chers_lectrices_dalibo`
- :ref:`licence_dalibo`
- https://www.dalibo.com/formation-postgresql-indexation-sql-avances
- https://public.dalibo.com/exports/formation/manuels/formations/perf2/perf2.handout.pdf

Objectifs
============

Cette formation présente en profondeur différentes techniques d’indexation,
ainsi que des fonctionnalités avancées offertes par PostgreSQL® :

- Connaître les index de PostgreSQL
- Maîtriser les techniques d’indexation avancées
- Maîtriser les types avancés
- Partitionner les données
- Corriger les mauvaises pratiques



Table des matières
====================

- https://public.dalibo.com/exports/formation/manuels/formations/perf2/perf2.handout.pdf


Techniques d'indexation, Indexation avancée
-----------------------------------------------

.. figure:: images/tdm_1.png
   :align: center


Extensions PostgreSQL pour la performance
-------------------------------------------

.. figure:: images/tdm_2.png
   :align: center


Partitionnement
------------------------------------------------------------------

.. figure:: images/tdm_3.png
   :align: center


Types avancés (hstore, JSON, XML, objets binaires), Fonctionnalités avancées pour la performance
---------------------------------------------------------------------------------------------------

.. figure:: images/tdm_4.png
   :align: center


Durée
========

La formation se déroule sur 2 jours (14 heures) en présentiel, ou sur
4 matinées (14 heures) à distance.

Thèmes abordés
===================

- Index btree, GiN, GiST, BRIN, Hash
- Index partiels, fonctionnels, couvrants
- Extensions PostgreSQL
- Types avancés (JSONB, binaires, range, etc.)

Public concerné
=================

Ce programme s’adresse aux utilisateurs confirmés et aux administrateurs
de bases de données (DBA) ayant déjà une bonne connaissance de PostgreSQL.

Pré-requis
=============

Avant la formation, nous demandons aux stagiaires de passer un test QCM
afin de vérifier les pré-requis suivants :

- Bonne connaissance du langage SQL
- Bonne connaissance de PostgreSQL

La formation Dalibo PERF1 ou DEVPG est conseillée.
