.. index::
   pair: DBA3 ; Dalibo Admin

.. _dalibo_admin_dba3:

=========================================
DBA3 Administration PostgreSQL Avancée
=========================================

- :ref:`chers_lectrices_dalibo`
- :ref:`licence_dalibo`
- https://www.dalibo.com/formation-postgresql-sauvegarde-restauration
- https://public.dalibo.com/exports/formation/manuels/formations/dba3/dba3.handout.pdf

Objectifs
============

Cette formation donne les clés pour :

- Sauvegarder et restaurer ses données
- Utiliser les outils de gestion de sauvegarde PITR
- Mettre en place un ou plusieurs serveurs de secours
- Répliquer physiquement et logiquement les données PostgreSQL
- Maîtriser les procédures de failover
- Reconstruire une instance



Table des matières
====================

- https://public.dalibo.com/exports/formation/manuels/formations/dba3/dba3.handout.pdf

PostgreSQL: Politique de sauvegarde, Sauvegarde physique à chaud et PITR, PostgreSQL: Outils de sauvegarde physique
---------------------------------------------------------------------------------------------------------------------

.. figure:: images/tdm_1.png
   :align: center


Architectures de Haute-Disponibilité
---------------------------------------

.. figure:: images/tdm_2.png
   :align: center


Réplication physique: fondamentaux, Réplication physique avancée, Les outils de réplication
-----------------------------------------------------------------------------------------------


.. figure:: images/tdm_3.png
   :align: center


Réplication logique
----------------------


.. figure:: images/tdm_4.png
   :align: center




Thèmes abordés
===================

- Point-in-Time Recovery
- Sauvegarde et restauration
- Outils : Pitrery, pgBackRest, Barman
- Hot Standby : installation et paramétrage
- Bascules : failover et failback
- Réplication logique
