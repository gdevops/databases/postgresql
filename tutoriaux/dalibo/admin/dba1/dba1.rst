.. index::
   pair: DBA1 ; Dalibo Admin


.. _dalibo_admin_dba1:

==================================
DBA1 Administration PostgreSQL
==================================

- :ref:`chers_lectrices_dalibo`
- :ref:`licence_dalibo`
- https://www.dalibo.com/formation-administration
- https://public.dalibo.com/exports/formation/manuels/formations/dba1/dba1.handout.html
- https://public.dalibo.com/exports/formation/manuels/formations/dba1/dba1.handout.pdf


Objectifs
===========

Cette formation a pour objectif de transmettre à des utilisateurs
occasionnels de la base PostgreSQL les connaissances et le savoir-faire
technique pour assurer les tâches suivantes :

- Installer PostgreSQL
- Comprendre les concepts de PostgreSQL
- Utiliser le serveur au quotidien
- Effectuer les tâches de maintenance


Table des matières
====================

- https://public.dalibo.com/exports/formation/manuels/formations/dba1/dba1.handout.pdf


PostgreSQL: historique et communauté, Découverte des fonctionnalités, Installation de PostgreSQL
------------------------------------------------------------------------------------------------------------------------------------------

.. figure:: images/tdm_1.png
   :align: center


Outils graphiques et console
--------------------------------------------------------

.. figure:: images/tdm_2.png
   :align: center


Tâches courantes, PostgreSQL: Politique de sauvegarde, PostgreSQL: Sauvegarde et restauration
----------------------------------------------------------------------------------------------

.. figure:: images/tdm_3.png
   :align: center


Supervision
---------------------------------------

.. figure:: images/tdm_4.png
   :align: center



Durée
=======

La formation se déroule sur 3 jours (21 heures) en présentiel, ou 5
matinées (17,5 heures) à distance.

Public concerné
===================

Ce programme s’adresse aux administrateurs systèmes et aux développeurs
désirant acquérir les compétences de gestion de PostgreSQL au quotidien.

Pré-requis
===========

Avant la formation, nous demandons aux stagiaires de passer un test QCM
afin de vérifier les pré-requis suivants :

- Connaissances minimales en système d’exploitation Linux en ligne de commande
- Notions de base en langage SQL


Modalités d’évaluation
========================

Lors de chaque module de formation, l’enseignement sera accompagné de
travaux pratiques et de QCM. Ces exercices et tests techniques permettront
ainsi de valider l’acquisition des connaissances par les stagiaires.
