.. index::
   pair: DBAADM ; Dalibo Admin

.. _dalibo_admin_dbaadm:

=========================================
DBAADM PostgreSQL pour DBA expérimentés
=========================================

- :ref:`chers_lectrices_dalibo`
- :ref:`licence_dalibo`
- https://public.dalibo.com/exports/formation/manuels/formations/dbaadm/dbaadm.handout.html
- https://public.dalibo.com/exports/formation/manuels/formations/dbaadm/dbaadm.handout.pdf
