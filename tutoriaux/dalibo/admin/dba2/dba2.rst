.. index::
   pair: DBA2 ; Dalibo Admin


.. _dalibo_admin_dba2:

=========================================
DBA2 Administration PostgreSQL Avancée
=========================================

- :ref:`chers_lectrices_dalibo`
- :ref:`licence_dalibo`
- https://www.dalibo.com/formation-postgresql-avance
- https://public.dalibo.com/exports/formation/manuels/formations/dba2/dba2.handout.html
- https://public.dalibo.com/exports/formation/manuels/formations/dba2/dba2.handout.pdf


Objectifs
===========

Cette formation a pour objectif de transmettre les connaissances et le
savoir-faire pour assurer les tâches suivantes :

- Maintenir un parc de serveurs en production
- Exploiter la puissance du moteur
- Garantir la sécurité des données
- Réagir face à une avarie (crash recovery)

Table des matières
====================

- https://public.dalibo.com/exports/formation/manuels/formations/dba2/dba2.handout.pdf


Architecture & fichiers de PostgreSQL, Configuration de PostgreSQL
----------------------------------------------------------------------

.. figure:: images/tdm_1.png
   :align: center


Mémoire et journalisation dans PostgreSQL
--------------------------------------------------------

.. figure:: images/tdm_2.png
   :align: center


Mécanique du moteur transactionnel & MVCC, VACUUM et autovaccuum
---------------------------------------------------------------------

.. figure:: images/tdm_3.png
   :align: center


Partitionnement
---------------------------------------

.. figure:: images/tdm_4.png
   :align: center


Sauvegarde physique à chaud et PITR, PostgreSQL: Gestion d'un sinistre
--------------------------------------------------------------------------

.. figure:: images/tdm_5.png
   :align: center

Durée
======

La formation se déroule sur 3 jours (21 heures) en présentiel, ou 5 matinées
(17,5 heures) à distance.

Public concerné
================

Ce programme s’adresse aux utilisateurs confirmés et aux administrateurs
de bases de données (DBA) ayant déjà une bonne connaissance de PostgreSQL.

Pré-requis
===============

Avant la formation, nous demandons aux stagiaires de passer un test QCM
afin de vérifier les pré-requis suivants :

- Connaissances minimales en système d’exploitation Linux
- Bonne connaissance du langage SQL
- Bonne connaissance de PostgreSQL

La formation Dalibo DBA1 est conseillée (Administration PostgreSQL).
