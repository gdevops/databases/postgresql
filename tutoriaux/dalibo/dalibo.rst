.. index::
   pair: PostgreSQL ; Dalibo


.. _dalibo_pg_tuto:

========================================================
Dalibo PostgreSQL tutorials
========================================================

- :ref:`chers_lectrices_dalibo`
- :ref:`licence_dalibo`
- https://www.dalibo.com/formations
- https://blog.dalibo.com/2020/05/28/e_formation.html
- https://pgsessions.com/
- https://x.com/pgsession
- https://pgsessions.com/archives.html

.. toctree::
   :maxdepth: 4

   lectrices/lectrices
   licence/licence
   dev/dev
   admin/admin
   performance/performance
