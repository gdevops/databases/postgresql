.. index::
   pair: PostgreSQL ; RealPython


.. _realpython_pg_tuto:

========================================================
RealPython PostgreSQL tutorials
========================================================

.. seealso::

   - https://realpython.com/python-sql-libraries/
