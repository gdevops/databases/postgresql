.. index::
   pair: pgpedia.info ; Tutorial

.. _pgpedia_info:

========================================================
pgpedia.info
========================================================

.. seealso::

   - https://pgpedia.info/
   - https://fljd.in//2020/11/18/quelques-outils-meconnus/





Quelques outils méconnus
============================

.. seealso::

   - https://fljd.in//2020/11/18/quelques-outils-meconnus/

Cette semaine, passait sur mon fil d’actualité Twitter une simple URL
pointant sur le site https://pgpedia.info.

Non loin d’être le seul à l’avoir remarqué, nous en parlions entre
collègues pour constater avec surprise que nous ne connaissions pas
cette mine d’or d’informations sur PostgreSQL.

Après y avoir perdu plusieurs heures, je me suis dit qu’un article sur
les quelques utilitaires que j’estime méconnus, pourrait être une bonne
conclusion de la semaine.
