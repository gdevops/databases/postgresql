
.. index::
   pair: SQL ; basics


.. _sql_basics:

===============
SQL basics
===============

:source: https://blog.timescale.com/blog/how-to-evaluate-your-data-directly-within-the-database-and-make-your-analysis-more-efficient/

The basics of a SQL SELECT command can be broken down like this 👇

::

    SELECT --columns, functions, aggregates, expressions that describe what you want to be shown in the results
    FROM --if selecting data from a table in your DB, you must define the table name here
    JOIN --join another table to the FROM statement table
        ON --a column that each table shares values
    WHERE --statement to filter results where a column or expression is equivalent to some statement
    GROUP BY --if SELECT or WHERE statement contains an aggregate, or if you want to group values on a column/expression, must include columns here
    HAVING --similar to WHERE, this keyword helps to filter results based upon columns or expressions specifically used with a GROUP BY query
    ORDER BY --allows you to specify the order in which your data is displayed
    LIMIT --lets you specify the number of rows you want displayed in the output


You can think of your queries as SELECTing data FROM your tables within
your database.
You can JOIN multiple tables together and specify WHERE your data needs
to be filtered or what it should be GROUPed BY.

Do you see what I did there 😋 ?

This is the beauty of SQL; these keyword's names were chosen to make
your queries intuitive.

Thankfully, most of PostgreSQL and SQL functionality follow this same
easy-to-read pattern. I have had the opportunity to teach myself many
programming languages throughout my career, and SQL is by far the easiest
to read, write, and construct.

This intuitive nature is another excellent reason why data munging in
PostgreSQL and TimescaleDB can be so efficient when compared to other methods.

Note that this list of keywords includes most of the ones you will need
to start selecting data with SQL; however, it is not exhaustive.
You will not need to use all these phrases for every query but likely
will need at least SELECT and FROM.
The queries in this blog post will always include these two keywords.

Additionally, the order of these keywords is specific. When building your
queries, you need to follow the order that I used above.
For any additional PostgreSQL commands you wish to use, you will have to
research where they fit in the order hierarchy and follow that accordingly.
