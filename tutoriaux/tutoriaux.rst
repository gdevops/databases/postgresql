.. index::
   pair: PostgreSQL ; Tutorials

.. _pg_tuto:

========================================================
PostgreSQL **tutorials**
========================================================

- https://www.dalibo.com/formations

.. toctree::
   :maxdepth: 3

   basics/basics
   dalibo/dalibo
   crunchydata/crunchydata
   pgpedia/pgpedia
   www.postgresqltutorial/www.postgresqltutorial
   stormatics/stormatics
   realpython/realpython
