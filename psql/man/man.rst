.. index::
   pair: psql ; man
   pair: psql ; meta-commands

.. _psql_man:

==================================================================
man **psql** + psql **meta-commands** + **shell-like** features
==================================================================


.. toctree::
   :maxdepth: 3

   psql
