.. index::
   pair: psql ; help

.. _psql_help:

========================================================
**psql** help
========================================================




which psql
============

::

    /usr/bin/psql

psql --help
=============

::

    psql --help

::

    psql est l'interface interactive de PostgreSQL.

    Usage :
      psql [OPTIONS]... [NOM_BASE [NOM_UTILISATEUR]]

    Options générales :
      -c, --command=COMMANDE
                      exécute une commande unique (SQL ou interne), puis quitte
      -d, --dbname=NOM_BASE
                      indique le nom de la base de données à laquelle se
                      connecter (par défaut : « pvergain »)
      -f, --file=FICHIER
                      exécute les commandes du fichier, puis quitte
      -l, --list      affiche les bases de données disponibles, puis quitte
      -v, --set=, --variable=NOM=VALEUR
                               configure la variable psql NOM en VALEUR
                               (e.g., -v ON_ERROR_STOP=1)
      -V, --version              affiche la version puis quitte
      -X, --no-psqlrc ne lit pas le fichier de démarrage (~/.psqlrc)
      -1 (« un »), --single-transaction
                      exécute dans une transaction unique (si non intéractif)
      -?, --help[=options]     affiche cette aide et quitte
          --help=commandes     liste les méta-commandes, puis quitte
          --help=variables     liste les variables spéciales, puis quitte

    Options d'entrée/sortie :
      -a, --echo-all  affiche les lignes du script
      -b, --echo-errors        affiche les commandes échouées
      -e, --echo-queries
                      affiche les commandes envoyées au serveur
      -E, --echo-hidden
                      affiche les requêtes engendrées par les commandes internes
      -L, --log-file=FICHIER
                      envoie les traces dans le fichier
      -n, --no-readline
                      désactive l'édition avancée de la ligne de commande
                      (readline)
      -o, --output=FICHIER
                      écrit les résultats des requêtes dans un fichier (ou
                      |tube)
      -q, --quiet     s'exécute silencieusement (pas de messages, uniquement le
                      résultat des requêtes)
      -s, --single-step
                      active le mode étape par étape (confirmation pour chaque
                      requête)
      -S, --single-line
                      active le mode ligne par ligne (EOL termine la commande
                      SQL)

    Options de formattage de la sortie :
      -A, --no-align  active le mode d'affichage non aligné des tables (-P
                      format=unaligned)
          --csv                mode d'affichage CSV (valeurs séparées par des virgules)

      -F, --field-separator=CHAINE
                      séparateur de champs pour un affichage non aligné
                      (par défaut : « | »)
      -H, --html      active le mode d'affichage HTML des tables (-P                  format=html)
      -P, --pset=VAR[=ARG]
                      initialise l'option d'impression VAR à ARG (voir la
                      commande \pset)
      -R, --record-separator=CHAINE
                      séparateur d'enregistrements pour un affichage non aligné
                      (par défaut : saut de ligne)
      -t, --tuples-only
                      affiche seulement les lignes (-P tuples_only)
      -T, --table-attr=TEXTE
                      initialise les attributs des balises HTML de tableau
                      (largeur, bordure) (-P tableattr=)
      -x, --expanded  active l'affichage étendu des tables (-P expanded)
      -z, --field-separator-zero
                      initialise le séparateur de champs pour un affichage non
                      aligné à l'octet zéro
      -0, --record-separator-zero
                      initialise le séparateur d'enregistrements pour un affichage
                      non aligné à l'octet zéro

    Options de connexion :
      -h, --host=HOTE nom d'hôte du serveur de la base de données ou répertoire
                      de la socket (par défaut : /var/run/postgresql)
      -p, --port=PORT port du serveur de la base de données (par défaut :
                      « 5432 »)
      -U, --username=NOM
                      nom d'utilisateur de la base de données (par défaut :
                      « pvergain »)
      -w, --no-password
                      ne demande jamais un mot de passe
      -W, --password  force la demande du mot de passe (devrait survenir
                      automatiquement)

    Pour en savoir davantage, saisissez « \? » (pour les commandes internes) ou
    « \help » (pour les commandes SQL) dans psql, ou consultez la section psql
    de la documentation de PostgreSQL.

    Rapporter les bogues à <pgsql-bugs@lists.postgresql.org>.
