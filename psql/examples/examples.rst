.. index::
   pair: psql ; \dt[S+] [MODÈLE] affiche la liste des tables
   pair: psql ; \di[S+] [MODÈLE] affiche la liste des index

.. _psql_examples:

========================================================
**psql** commands examples
========================================================

Connexion
========================================

::

    psql -U intranet -h localhost -p 5433
    psql postgresql://<user>:<password>@<host>:<port>/<database_name>
    psql postgresql://intranet:<password>@database-host:5432/db_intranet

Example 1 createuser -SDr appdev
===================================

::

    createuser -SDr appdev

::

    createdb -O appdev db_dev

::

    psql -d db_dev -c 'create extension btree_gist'

        CREATE EXTENSION

::


    psql -d db_intranet -U intranet -h X.X.X.X  -p 5432



Example 2
==========

::

    sudo su - postgres

    [sudo] Mot de passe de pvergain :

::

    Pas de répertoire, connexion avec HOME=/

::

    $ psql

    psql (12.2 (Ubuntu 12.2-2.pgdg18.04+1))
    Saisissez « help » pour l'aide.

    postgres=# CREATE DATABASE db_intranet WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'fr_FR.utf8' LC_CTYPE = 'fr_FR.utf8';
    CREATE DATABASE
    postgres=# CREATE ROLE intranet;
    CREATE ROLE
    postgres=# ALTER ROLE intranet WITH SUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS;
    ALTER ROLE
    postgres=# ALTER ROLE intranet WITH LOGIN PASSWORD '<password>';
    ALTER ROLE
    postgres=#


Example 3 (psql -d db_intranet -U intranet)
===============================================

::

    pg_restore -d db_intranet -U intranet --if-exists -c db_intranet.sql

::

    pvergain@pvergain-MS-7721 psql -d db_intranet -U intranet

::

    L'affichage étendu est utilisé automatiquement.
    L'affichage de null est « ¤ ».
    Le style de ligne est unicode.
    Le style de bordure Unicode est « single ».
    Le style de ligne Unicode est « single ».
    Le style d'en-tête Unicode est « double ».
    SET
    psql (12.2 (Ubuntu 12.2-2.pgdg18.04+1))
    Saisissez « help » pour l'aide.

    intranet@db_intranet=#  \dt
                              Liste des relations
     Schéma │                  Nom                   │ Type  │ Propriétaire
    ════════╪════════════════════════════════════════╪═══════╪══════════════
     public │ article                                │ table │ intranet
     public │ article_achat                          │ table │ intranet


.. _liste_des_tables:

\dt[S+] [MODÈLE]     affiche la liste des tables
========================================================

::

                                   Liste des relations
     Schéma |                 Nom                 |        Type        | Propriétaire
    --------+-------------------------------------+--------------------+--------------
     public | auth_group                          | table              | log
     public | auth_group_permissions              | table              | log
     public | auth_permission                     | table              | log
     public | auth_user                           | table              | log
     public | auth_user_groups                    | table              | log
     public | auth_user_user_permissions          | table              | log
     public | authtoken_token                     | table              | log
     public | django_admin_log                    | table              | log
     public | django_content_type                 | table              | log
     public | django_migrations                   | table              | log
     public | django_session                      | table              | log
     public | django_sql_dashboard_dashboard      | table              | log
     public | django_sql_dashboard_dashboardquery | table              | log
     public | log                                 | table partitionnée | log
     public | log_2018                            | table              | log
     public | log_2019                            | table              | log
     public | log_2020                            | table              | log
     public | log_2021                            | table              | log
     public | log_2022                            | table              | log
     public | log_2023                            | table              | log
     public | log_2024                            | table              | log
     public | log_2025                            | table              | log
     public | log_2026                            | table              | log
     public | log_2027                            | table              | log
     public | log_2028                            | table              | log
     public | log_2029                            | table              | log
     public | log_2030                            | table              | log
     public | log_2031                            | table              | log
     public | log_2032                            | table              | log
     public | pg_timepart_partitionconfig         | table              | log
     public | pg_timepart_partitionlog            | table              | log
    (31 lignes)


\di[S+] [MODÈLE] affiche la liste des index
====================================================

::

    \di


::

                                                                  Liste des relations
     Schéma |                              Nom                               |       Type        | Propriétaire |                Table
    --------+----------------------------------------------------------------+-------------------+--------------+-------------------------------------
     public | argument4_index                                                | index partitionné | log          | log
     public | auth_group_name_a6ea08ec_like                                  | index             | log          | auth_group
     public | auth_group_name_key                                            | index             | log          | auth_group
     public | auth_group_permissions_group_id_b120cbf9                       | index             | log          | auth_group_permissions
     public | auth_group_permissions_group_id_permission_id_0cd325b0_uniq    | index             | log          | auth_group_permissions
     public | auth_group_permissions_permission_id_84c5c92e                  | index             | log          | auth_group_permissions
     public | auth_group_permissions_pkey                                    | index             | log          | auth_group_permissions
     public | auth_group_pkey                                                | index             | log          | auth_group
     public | auth_permission_content_type_id_2f476e4b                       | index             | log          | auth_permission
     public | auth_permission_content_type_id_codename_01ab375a_uniq         | index             | log          | auth_permission
     public | auth_permission_pkey                                           | index             | log          | auth_permission
     public | auth_user_groups_group_id_97559544                             | index             | log          | auth_user_groups



\dg[S+]  [MODÈLE] affiche la liste des rôles (utilisateurs)
================================================================

::

    \dg

::

                                           Liste des rôles
     Nom du rôle |                                    Attributs
    -------------+---------------------------------------------------------------------------------
     intranet    |
     keycloak    |
     log         |
     log_stats   |
     postgres    | Superutilisateur, Créer un rôle, Créer une base, Réplication, Contournement RLS
