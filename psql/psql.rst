.. index::
   pair: psql ; PostgreSQL interactive terminal

.. _psql_command:

========================================================
**psql** command (PostgreSQL interactive terminal)
========================================================

- https://pgpedia.info/p/psql.html
- https://pgpedia.info/s/slash-command.html

Description
=============

psql is PostgreSQL's standard command line client application and is a
standard part of most PostgreSQL installations.

It can be used both for terminal-based text-only interaction with a
PostgreSQL server, and also for facilitating automated script-based tasks.

psql has been a core part of the PostgreSQL project since its inception
and is usually present in PostgreSQL installations.

It can also be installed as a standalone application, and many operating
systems/distributions provide separate packages for the PostgreSQL server
itself and for the core client applications. psql has of course been
ported to Windows, but with certain limitations, particularly readline support.

psql can connect to a PostgreSQL instance on the same server via a Unix
domain socket if available, or connect TCP/IP to PostgreSQL running either
on the local or a remote server.



::

    psql -U postgres -d postgres
    psql postgresql://<user>:<password>@<host>:<port>/<database_name>
    psql postgresql://intranet:<password>@database-host:5432/db_intranet


.. toctree::
   :maxdepth: 3

   help/help
   man/man
   examples/examples
