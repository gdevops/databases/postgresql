

========================================================
PostgreSQL 17 installation on **Debian 12 bookworm**
========================================================

- https://cloudspinx.com/how-to-install-postgresql-17-on-debian/
- :ref:`service_postgresql`


Before tree /etc/postgresql
===============================

::

    ➜ tree /etc/postgresql
    /etc/postgresql
    ├── 15
    │   └── main
    │       ├── conf.d
    │       ├── environment
    │       ├── pg_ctl.conf
    │       ├── pg_hba.conf
    │       ├── pg_ident.conf
    │       ├── postgresql.conf
    │       └── start.conf
    └── 17
        └── main
            ├── conf.d
            ├── environment
            ├── pg_ctl.conf
            ├── pg_hba.conf
            ├── pg_ident.conf
            ├── postgresql.conf
            └── start.conf
            
pg_dropcluster 15 main
==========================


::

    sudo pg_dropcluster 15 main

After tree /etc/postgresql
=================================

::

    ➜ tree /etc/postgresql
    /etc/postgresql
    ├── 15
    │   └── main
    │       ├── conf.d
    │       ├── environment
    │       ├── pg_ctl.conf
    │       ├── pg_hba.conf
    │       ├── pg_ident.conf
    │       ├── postgresql.conf
    │       └── start.conf
    └── 17
        └── main
            ├── conf.d
            ├── environment
            ├── pg_ctl.conf
            ├── pg_hba.conf
            ├── pg_ident.conf
            ├── postgresql.conf
            └── start.conf

    7 directories, 12 files


sudo -u postgres psql -c "SELECT version();"
===================================================

::

    ➜ sudo -u postgres psql -c "SELECT version();"
                                                           version                                                       
    ---------------------------------------------------------------------------------------------------------------------
     PostgreSQL 17.4 (Debian 17.4-1.pgdg120+2) on x86_64-pc-linux-gnu, compiled by gcc (Debian 12.2.0-14) 12.2.0, 64-bit
    (1 ligne)


