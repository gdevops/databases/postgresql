begin;
BEGIN
drop table if exists tstz;
DROP TABLE
create table tstz(ts timestamp, tstz timestamptz);
CREATE TABLE
set timezone to 'Europe/Paris';
SET
select now();
              now              
-------------------------------
 2020-04-30 18:35:12.909262+02
(1 ligne)

insert into tstz values(now(), now());
INSERT 0 1
set timezone to 'Pacific/Tahiti';
SET
select now();
              now              
-------------------------------
 2020-04-30 06:35:12.909262-10
(1 ligne)

insert into tstz values(now(), now());
INSERT 0 1
set timezone to 'Europe/Paris';
SET
table tstz;
             ts             |             tstz              
----------------------------+-------------------------------
 2020-04-30 18:35:12.909262 | 2020-04-30 18:35:12.909262+02
 2020-04-30 06:35:12.909262 | 2020-04-30 18:35:12.909262+02
(2 lignes)

set timezone to 'Pacific/Tahiti';
SET
table tstz;
             ts             |             tstz              
----------------------------+-------------------------------
 2020-04-30 18:35:12.909262 | 2020-04-30 06:35:12.909262-10
 2020-04-30 06:35:12.909262 | 2020-04-30 06:35:12.909262-10
(2 lignes)

commit;
COMMIT
