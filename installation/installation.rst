.. index::
   pair: Installation ; PostgreSQL

.. _postgresql_installation:

========================================================
PostgreSQL **installation**
========================================================

.. toctree::
   :maxdepth: 3


   17/17
   15/15
   12/12
   timezones/timezones
   docker/docker
