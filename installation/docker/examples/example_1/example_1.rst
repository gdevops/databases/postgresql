
 Examples

.. _postgresql_docker_example_1:

============================
PostgreSQL docker example 1
============================



Dockerfile
===========

.. literalinclude:: docker_db/Dockerfile
   :linenos:


docker-compose_dev.yml
=========================

.. literalinclude:: docker-compose_dev.yml
   :linenos:


docker-compose_dev.yml
=========================

.. literalinclude:: docker-compose_production.yml
   :linenos:
