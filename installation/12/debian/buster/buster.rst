.. index::
   pair: Debian ; buster


.. _postgres_debian_10_buster:

========================================================
PostgreSQL installation on **Debian 10 buster**
========================================================

- https://www.postgresql.org/download/linux/debian/


Caractéristiques de la machine GNU/Linux
=============================================


This machine is a Debian buster machine.


::

    inxi -F


::

    System:    Host: uc045 Kernel: 4.19.0-8-amd64 x86_64 bits: 64 Desktop: Cinnamon 3.8.8 Distro: Debian GNU/Linux 10 (buster)
    Machine:   Type: Desktop System: HP product: HP EliteDesk 800 G5 Desktop Mini v: N/A serial: <root required>
               Mobo: HP model: 8594 v: KBC Version 08.95.00 serial: <root required> UEFI: HP v: R21 Ver. 02.04.02 date: 12/27/2019
    CPU:       Topology: 6-Core model: Intel Core i7-8700 bits: 64 type: MT MCP L2 cache: 12.0 MiB
               Speed: 853 MHz min/max: 800/4600 MHz Core speeds (MHz): 1: 892 2: 886 3: 842 4: 842 5: 842 6: 886 7: 878 8: 808
               9: 888 10: 844 11: 896 12: 899
    Graphics:  Device-1: Intel UHD Graphics 630 driver: i915 v: kernel
               Display: x11 server: X.Org 1.20.4 driver: modesetting unloaded: fbdev,vesa
               resolution: 1920x1080~60Hz, 1920x1080~60Hz
               OpenGL: renderer: Mesa DRI Intel UHD Graphics 630 (Coffeelake 3x8 GT2) v: 4.5 Mesa 18.3.6
    Audio:     Device-1: Intel Cannon Lake PCH cAVS driver: snd_hda_intel
               Sound Server: ALSA v: k4.19.0-8-amd64
    Network:   Device-1: Intel Ethernet I219-LM driver: e1000e
               IF: eno1 state: up speed: 100 Mbps duplex: full mac: e8:d8:d1:54:42:49
               Device-2: Intel driver: N/A
               IF-ID-1: br-eab59556caad state: down mac: 02:42:d1:a3:0e:4a
               IF-ID-2: docker0 state: down mac: 02:42:73:d3:cc:ae
    Drives:    Local Storage: total: 465.76 GiB used: 26.98 GiB (5.8%)
               ID-1: /dev/nvme0n1 vendor: Crucial model: CT500P1SSD8 size: 465.76 GiB
    Partition: ID-1: / size: 441.18 GiB used: 26.92 GiB (6.1%) fs: ext4 dev: /dev/dm-0
               ID-2: /boot size: 236.3 MiB used: 58.8 MiB (24.9%) fs: ext2 dev: /dev/nvme0n1p2
               ID-3: swap-1 size: 15.78 GiB used: 0 KiB (0.0%) fs: swap dev: /dev/dm-1
    Sensors:   System Temperatures: cpu: 38.0 C mobo: N/A
               Fan Speeds (RPM): N/A
    Info:      Processes: 289 Uptime: 1h 46m Memory: 15.45 GiB used: 3.28 GiB (21.2%) Shell: bash inxi: 3.0.32


Create the etc/apt/sources.list.d/pgdg.list file
=====================================================

Create the etc/apt/sources.list.d/pgdg.list file and add this line::

    deb http://apt.postgresql.org/pub/repos/apt/ buster-pgdg main



tree /etc/postgresql
======================

::

    tree /etc/postgresql

::

    root@uc045:/etc/apt/sources.list.d# tree /etc/postgresql
    /etc/postgresql
    └── 12
        └── main
            ├── conf.d
            ├── environment
            ├── pg_ctl.conf
            ├── pg_hba.conf
            ├── pg_ident.conf
            ├── postgresql.conf
            └── start.conf

    3 directories, 6 files


.. _main_postgresql_conf_12:

**main/postgresql.conf**
---------------------------

.. literalinclude::  main/postgresql.conf
   :linenos:


.. _postgres_setting_utc_timezone:

Setting the UTC **timezone** parameter in main/postgresql.conf
-------------------------------------------------------------------

.. literalinclude::  main/postgresql.conf
   :linenos:
   :lines: 647-651

::

    diff --git a/postgresql.conf b/postgresql.conf
    index 3aeef81..ce69bdf 100644
    --- a/postgresql.conf
    +++ b/postgresql.conf
    @@ -536,7 +536,7 @@ log_line_prefix = '%m [%p] %q%u@%d '                # special values:
     #log_temp_files = -1                   # log temporary files equal or larger
                                            # than the specified size in kilobytes;
                                            # -1 disables, 0 logs all temp files
    -log_timezone = 'Europe/Paris'
    +log_timezone = 'UTC'

     #------------------------------------------------------------------------------
     # PROCESS TITLE
    @@ -648,7 +648,7 @@ stats_temp_directory = '/var/run/postgresql/12-main.pg_stat_tmp'

     datestyle = 'iso, dmy'
     #intervalstyle = 'postgres'
    -timezone = 'Europe/Paris'
    +timezone = 'UTC'
     #timezone_abbreviations = 'Default'     # Select the set of available time zone
                                            # abbreviations.  Currently, there are
                                            #   Default



After setting a new timezone, the Postgresql service must be restarted::

    sudo service postgresql restart


.. _pg_hba_conf_12:

**main/pg_hba.conf**
---------------------------

.. literalinclude::  main/pg_hba.conf
   :linenos:



dpkg -l | grep postgres
==========================

::

    dpkg -l | grep postgres

::

    ii  pgdg-keyring                          2018.2                                       all          keyring for apt.postgresql.org
    ii  postgresql-12                         12.2-2.pgdg100+1                             amd64        object-relational SQL database, version 12 server
    ii  postgresql-client-12                  12.2-2.pgdg100+1                             amd64        front-end programs for PostgreSQL 12
    ii  postgresql-client-common              213.pgdg100+1                                all          manager for multiple PostgreSQL client versions
    ii  postgresql-common                     213.pgdg100+1                                all          PostgreSQL database-cluster manager


netstat -nl | grep postgres
==============================

::

    netstat -nl | grep postgres

::

    root@uc045:/etc/apt/sources.list.d# netstat -nl | grep postgres
    unix  2      [ ACC ]     STREAM     LISTENING     23458    /var/run/postgresql/.s.PGSQL.5432


service postgresql status
==========================

::

    root@uc045:/etc/apt/sources.list.d# service postgresql status

::

    ● postgresql.service - PostgreSQL RDBMS
       Loaded: loaded (/lib/systemd/system/postgresql.service; enabled; vendor preset: enabled)
       Active: active (exited) since Tue 2020-03-24 10:22:25 CET; 1h 57min ago
      Process: 883 ExecStart=/bin/true (code=exited, status=0/SUCCESS)
     Main PID: 883 (code=exited, status=0/SUCCESS)

    mars 24 10:22:25 uc045 systemd[1]: Starting PostgreSQL RDBMS...
    mars 24 10:22:25 uc045 systemd[1]: Started PostgreSQL RDBMS.


nxi -Fxxz
============

::

    root@uc045:/etc/apt/sources.list.d# inxi -Fxxz

::

    System:    Host: uc045 Kernel: 4.19.0-8-amd64 x86_64 bits: 64 compiler: gcc v: 8.3.0 Console: tty 6 dm: LightDM
               Distro: Debian GNU/Linux 10 (buster)
    Machine:   Type: Desktop System: HP product: HP EliteDesk 800 G5 Desktop Mini v: N/A serial: <filter> Chassis: type: 6
               serial: <filter>
               Mobo: HP model: 8594 v: KBC Version 08.95.00 serial: <filter> UEFI: HP v: R21 Ver. 02.04.02 date: 12/27/2019
    CPU:       Topology: 6-Core model: Intel Core i7-8700 bits: 64 type: MT MCP arch: Kaby Lake rev: A L1 cache: 384 KiB
               L2 cache: 12.0 MiB L3 cache: 12.0 MiB
               flags: lm nx pae sse sse2 sse3 sse4_1 sse4_2 ssse3 vmx bogomips: 76608
               Speed: 1745 MHz min/max: 800/4600 MHz Core speeds (MHz): 1: 1745 2: 1027 3: 965 4: 1219 5: 943 6: 1827 7: 1156
               8: 1429 9: 1287 10: 1401 11: 1527 12: 1700
    Graphics:  Device-1: Intel UHD Graphics 630 vendor: Hewlett-Packard driver: i915 v: kernel bus ID: 00:02.0 chip ID: 8086:3e92
               Display: server: X.org 1.20.4 driver: modesetting unloaded: fbdev,vesa tty: 211x54
               Message: Advanced graphics data unavailable in console for root.
    Audio:     Device-1: Intel Cannon Lake PCH cAVS vendor: Hewlett-Packard driver: snd_hda_intel v: kernel bus ID: 00:1f.3
               chip ID: 8086:a348
               Sound Server: ALSA v: k4.19.0-8-amd64
    Network:   Device-1: Intel Ethernet I219-LM vendor: Hewlett-Packard driver: e1000e v: 3.2.6-k port: efa0 bus ID: 00:1f.6
               chip ID: 8086:15bb
               IF: eno1 state: up speed: 100 Mbps duplex: full mac: <filter>
               Device-2: Intel driver: N/A port: efa0 bus ID: 02:00.0 chip ID: 8086:2723
               IF-ID-1: br-eab59556caad state: down mac: <filter>
               IF-ID-2: docker0 state: down mac: <filter>
    Drives:    Local Storage: total: 465.76 GiB used: 26.98 GiB (5.8%)
               ID-1: /dev/nvme0n1 vendor: Crucial model: CT500P1SSD8 size: 465.76 GiB speed: 31.6 Gb/s lanes: 4 serial: <filter>
    Partition: ID-1: / size: 441.18 GiB used: 26.92 GiB (6.1%) fs: ext4 dev: /dev/dm-0
               ID-2: /boot size: 236.3 MiB used: 58.8 MiB (24.9%) fs: ext2 dev: /dev/nvme0n1p2
               ID-3: swap-1 size: 15.78 GiB used: 0 KiB (0.0%) fs: swap dev: /dev/dm-1
    Sensors:   System Temperatures: cpu: 36.0 C mobo: N/A
               Fan Speeds (RPM): N/A
    Info:      Processes: 296 Uptime: 1h 58m Memory: 15.45 GiB used: 3.34 GiB (21.6%) Init: systemd v: 241 runlevel: 5 Compilers:
               gcc: 8.3.0 alt: 8 Shell: bash v: 5.0.3 running in: tty 6 inxi: 3.0.32



Configuring Postgres for Use
==============================

The postgres user
-------------------

While PostgreSQL become installed, a system user account named postgres was also
created with an identical user account in postgres.

**By default, the postgres user account isn't configured with a password**, so it
isn't viable to log into the server the use of the postgres user account without
first creating a password for it.

This postgres account has an all-access pass on your PostgreSQL database server,
permission-wise.

The postgres user account has similarities to the **sa account in SQL server**.

The postgres database
------------------------

**PostgreSQL is installed with a default database postgres**.

For the most part, we use the postgres database for administration functions,
and create new databases on the PostgreSQL server to suit our needs.

The psql Command Line Utility
---------------------------------

PostgreSQL consists of psql, a command line application for managing your
databases and server.

While a GUI-based software such as pgadmin3 is often  less complicated to use
in the daily task, the command line utilty psql is always handy.

psql gives total control of your postgres system from the terminal, together
with the ability to execute SQL queries.

We will use psql to perform our preliminary configuration and to create an
initial database super user.

Login and Connect as Default User
=====================================

For most systems, the default Postgres user is postgres and a password is not
required for authentication.

Thus, to add a password, we must first login and connect as the postgres user.

::

    $ sudo -u postgres psql

::

    [sudo] Mot de passe de pvergain :

::

    psql (12.2 (Ubuntu 12.2-2.pgdg18.04+1))
    Saisissez « help » pour l'aide.

    postgres=#

Changing the Password
=========================

With a connection now established to Postgres at the psql prompt, issue the
ALTER USER command to change the password for the postgres user:

::

    postgres=# alter user postgres password 'myPassword';

::

    ALTER ROLE


.. warning:: Il faut bien mettre les "'" à 'myPassword'

If successful, Postgres will output a confirmation of ALTER ROLE as seen above.

Finally, exit the psql client by using the \q command::

    postgres=# \q

You’re all done.

The default postgres user now has a password associated with the account for
use in your other applications.
