
========================================================
PostgreSQL installation on **linuxmint**
========================================================

.. seealso::

   - https://r00t4bl3.com/post/how-to-install-postgresql-12-on-linux-mint-19-tara-19-1-tessa-19-2-tina-19-3-tricia
   - https://wiki.postgresql.org/wiki/Apt


.. toctree::
   :maxdepth: 3

   tara/tara
