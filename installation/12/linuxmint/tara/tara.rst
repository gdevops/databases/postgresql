.. index::
   pair: Tara ; PostgreSQL 12
   pair: Command; netstat -nl


.. _postgresql_tara:

========================================================
PostgreSQL installation on **linuxmint tara 19**
========================================================

.. seealso::

   - https://r00t4bl3.com/post/how-to-install-postgresql-12-on-linux-mint-19-tara-19-1-tessa-19-2-tina-19-3-tricia
   - https://wiki.postgresql.org/wiki/Apt





Linux/mint tara
==================

.. seealso::

   - :ref:`linux_mint`
   - :ref:`linux_mint_19`


/etc/apt/sources.list.d/postgresql.list
=========================================

::

    sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main" > \
    /etc/apt/sources.list.d/postgresql.list'


::


    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

::

    OK


sudo apt update
================

::

    sudo apt update

::

    Atteint :1 http://archive.canonical.com/ubuntu bionic InRelease
    Réception de :2 http://security.ubuntu.com/ubuntu bionic-security InRelease [88,7 kB]
    Atteint :3 http://packages.microsoft.com/repos/vscode stable InRelease
    Réception de :4 http://apt.postgresql.org/pub/repos/apt bionic-pgdg InRelease [46,3 kB]
    Atteint :5 http://archive.ubuntu.com/ubuntu bionic InRelease
    Réception de :6 http://archive.ubuntu.com/ubuntu bionic-updates InRelease [88,7 kB]
    Atteint :7 http://ppa.launchpad.net/git-core/ppa/ubuntu bionic InRelease
    Ign :8 http://packages.linuxmint.com tricia InRelease
    Réception de :9 http://archive.ubuntu.com/ubuntu bionic-backports InRelease [74,6 kB]
    Atteint :10 https://apt.enpass.io stable InRelease
    Atteint :11 https://brave-browser-apt-release.s3.brave.com bionic InRelease
    Atteint :12 http://packages.linuxmint.com tricia Release
    Réception de :13 https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs vscodium InRelease [3 828 B]
    Atteint :14 https://repo.fortinet.com/repo/ubuntu /bionic InRelease
    Réception de :15 http://apt.postgresql.org/pub/repos/apt bionic-pgdg/main i386 Packages [178 kB]
    Réception de :16 http://apt.postgresql.org/pub/repos/apt bionic-pgdg/main amd64 Packages [179 kB]
    658 ko réceptionnés en 2s (291 ko/s)
    Lecture des listes de paquets... Fait
    Construction de l'arbre des dépendances
    Lecture des informations d'état... Fait
    Tous les paquets sont à jour.


sudo apt upgrade
=================


::

    sudo apt upgrade

::

    Lecture des listes de paquets... Fait
    Construction de l'arbre des dépendances
    Lecture des informations d'état... Fait
    Calcul de la mise à jour... Fait
    0 mis à jour, 0 nouvellement installés, 0 à enlever et 0 non mis à jour.



sudo apt install postgresql-12
===============================


::

    sudo apt install postgresql-12

::

    Lecture des listes de paquets... Fait
    Construction de l'arbre des dépendances
    Lecture des informations d'état... Fait
    Les paquets supplémentaires suivants seront installés :
    libpq5 pgdg-keyring postgresql-client-12 postgresql-client-common postgresql-common
    Paquets suggérés :
    postgresql-doc-12 libjson-perl
    Paquets recommandés :
    sysstat
    Les NOUVEAUX paquets suivants seront installés :
    libpq5 pgdg-keyring postgresql-12 postgresql-client-12 postgresql-client-common postgresql-common
    0 mis à jour, 6 nouvellement installés, 0 à enlever et 0 non mis à jour.
    Il est nécessaire de prendre 16,4 Mo dans les archives.
    Après cette opération, 53,9 Mo d'espace disque supplémentaires seront utilisés.
    Souhaitez-vous continuer ? [O/n] O

::


    Réception de :1 http://apt.postgresql.org/pub/repos/apt bionic-pgdg/main amd64 libpq5 amd64 12.2-2.pgdg18.04+1 [172 kB]
    Réception de :2 http://apt.postgresql.org/pub/repos/apt bionic-pgdg/main amd64 pgdg-keyring all 2018.2 [10,7 kB]
    Réception de :3 http://apt.postgresql.org/pub/repos/apt bionic-pgdg/main amd64 postgresql-client-common all 213.pgdg18.04+1 [87,2 kB]
    Réception de :4 http://apt.postgresql.org/pub/repos/apt bionic-pgdg/main amd64 postgresql-client-12 amd64 12.2-2.pgdg18.04+1 [1 392 kB]
    Réception de :5 http://apt.postgresql.org/pub/repos/apt bionic-pgdg/main amd64 postgresql-common all 213.pgdg18.04+1 [236 kB]
    Réception de :6 http://apt.postgresql.org/pub/repos/apt bionic-pgdg/main amd64 postgresql-12 amd64 12.2-2.pgdg18.04+1 [14,5 MB]
    16,4 Mo réceptionnés en 1s (11,7 Mo/s)
    Préconfiguration des paquets...
    Sélection du paquet libpq5:amd64 précédemment désélectionné.
    (Lecture de la base de données... 627227 fichiers et répertoires déjà installés.)
    Préparation du dépaquetage de .../0-libpq5_12.2-2.pgdg18.04+1_amd64.deb ...
    Dépaquetage de libpq5:amd64 (12.2-2.pgdg18.04+1) ...
    Sélection du paquet pgdg-keyring précédemment désélectionné.
    Préparation du dépaquetage de .../1-pgdg-keyring_2018.2_all.deb ...
    Dépaquetage de pgdg-keyring (2018.2) ...
    Sélection du paquet postgresql-client-common précédemment désélectionné.
    Préparation du dépaquetage de .../2-postgresql-client-common_213.pgdg18.04+1_all.deb ...
    Dépaquetage de postgresql-client-common (213.pgdg18.04+1) ...
    Sélection du paquet postgresql-client-12 précédemment désélectionné.
    Préparation du dépaquetage de .../3-postgresql-client-12_12.2-2.pgdg18.04+1_amd64.deb ...
    Dépaquetage de postgresql-client-12 (12.2-2.pgdg18.04+1) ...
    Sélection du paquet postgresql-common précédemment désélectionné.
    Préparation du dépaquetage de .../4-postgresql-common_213.pgdg18.04+1_all.deb ...
    Ajout de « détournement de /usr/bin/pg_config en /usr/bin/pg_config.libpq-dev par postgresql-common »
    Dépaquetage de postgresql-common (213.pgdg18.04+1) ...
    Sélection du paquet postgresql-12 précédemment désélectionné.
    Préparation du dépaquetage de .../5-postgresql-12_12.2-2.pgdg18.04+1_amd64.deb ...
    Dépaquetage de postgresql-12 (12.2-2.pgdg18.04+1) ...
    Paramétrage de libpq5:amd64 (12.2-2.pgdg18.04+1) ...
    Paramétrage de pgdg-keyring (2018.2) ...
    Removing apt.postgresql.org key from trusted.gpg: OK
    Paramétrage de postgresql-client-common (213.pgdg18.04+1) ...
    Paramétrage de postgresql-common (213.pgdg18.04+1) ...

Ajout de l'utilisateur postgres au groupe **ssl-cert**
=========================================================

::


    Ajout de l'utilisateur postgres au groupe ssl-cert

    Creating config file /etc/postgresql-common/createcluster.conf with new version
    Building PostgreSQL dictionaries from installed myspell/hunspell packages...
    en_us
    fr
    Removing obsolete dictionary files:
    Created symlink /etc/systemd/system/multi-user.target.wants/postgresql.service → /lib/systemd/system/postgresql.service.
    Paramétrage de postgresql-client-12 (12.2-2.pgdg18.04+1) ...
    update-alternatives: utilisation de « /usr/share/postgresql/12/man/man1/psql.1.gz » pour fournir « /usr/share/man/man1/psql.1.gz » (psql.1.gz) en mode automatique
    Paramétrage de postgresql-12 (12.2-2.pgdg18.04+1) ...

Creating new PostgreSQL cluster 12/main
=========================================

::

    Creating new PostgreSQL cluster 12/main ...


/usr/lib/postgresql/12/bin/initdb -D /var/lib/postgresql/12/main --auth-local peer --auth-host md5
======================================================================================================

::

    /usr/lib/postgresql/12/bin/initdb -D /var/lib/postgresql/12/main --auth-local peer --auth-host md5

    **Les fichiers de ce système de bases de données appartiendront à l'utilisateur « postgres ».**
    Le processus serveur doit également lui appartenir.

    L'instance sera initialisée avec la locale « fr_FR.UTF-8 ».
    L'encodage par défaut des bases de données a été configuré en conséquence
    avec « UTF8 ».
    La configuration de la recherche plein texte a été initialisée à « french ».

    Les sommes de contrôle des pages de données sont désactivées.

    correction des droits sur le répertoire existant /var/lib/postgresql/12/main... ok
    création des sous-répertoires... ok
    sélection de l'implémentation de la mémoire partagée dynamique...posix
    sélection de la valeur par défaut pour max_connections... 100
    sélection de la valeur par défaut pour shared_buffers... 128MB
    sélection du fuseau horaire par défaut... Europe/Paris
    création des fichiers de configuration... ok
    lancement du script bootstrap...ok
    exécution de l'initialisation après bootstrap... ok
    synchronisation des données sur disque... ok


pg_ctlcluster 12 main start
==============================

::

    Succès. Vous pouvez maintenant lancer le serveur de bases de données en utilisant :

    pg_ctlcluster 12 main start

    Ver Cluster Port Status Owner    Data directory              Log file

    12  main    5433 down   postgres /var/lib/postgresql/12/main /var/log/postgresql/postgresql-12-main.log

    update-alternatives: utilisation de « /usr/share/postgresql/12/man/man1/postmaster.1.gz » pour fournir « /usr/share/man/man1/postmaster.1.gz » (postmaster.1.gz) en mode automatique
    Traitement des actions différées (« triggers ») pour libc-bin (2.27-3ubuntu1) ...
    Traitement des actions différées (« triggers ») pour systemd (237-3ubuntu10.39) ...
    Traitement des actions différées (« triggers ») pour man-db (2.8.3-2ubuntu0.1) ...
    Traitement des actions différées (« triggers ») pour ureadahead (0.100.0-21) ...
    ureadahead will be reprofiled on next reboot


ps -ef | grep postgre
======================

::

    postgres  1662     1  0 07:11 ?        00:00:00 /usr/lib/postgresql/12/bin/postgres -D /var/lib/postgresql/12/main -c config_file=/etc/postgresql/12/main/postgresql.conf
    postgres  1771  1662  0 07:11 ?        00:00:00 postgres: 12/main: checkpointer
    postgres  1772  1662  0 07:11 ?        00:00:00 postgres: 12/main: background writer
    postgres  1773  1662  0 07:11 ?        00:00:00 postgres: 12/main: walwriter
    postgres  1774  1662  0 07:11 ?        00:00:00 postgres: 12/main: autovacuum launcher
    postgres  1775  1662  0 07:11 ?        00:00:00 postgres: 12/main: stats collector
    postgres  1776  1662  0 07:11 ?        00:00:00 postgres: 12/main: logical replication launcher


tree /etc/postgresql
=======================

::

    /etc/postgresql
    └── 12
        └── main
            ├── conf.d
            ├── environment
            ├── pg_ctl.conf
            ├── pg_hba.conf
            ├── pg_ident.conf
            ├── postgresql.conf
            └── start.conf

dpkg -l | grep postgres
===========================

::

    dpkg -l | grep postgres

::

    ii  pgdg-keyring                          2018.2                      all          keyring for apt.postgresql.org
    ii  postgresql-12                         12.2-2.pgdg18.04+1          amd64        object-relational SQL database, version 12 server
    ii  postgresql-client-12                  12.2-2.pgdg18.04+1          amd64        front-end programs for PostgreSQL 12
    ii  postgresql-client-common              213.pgdg18.04+1             all          manager for multiple PostgreSQL client versions
    ii  postgresql-common                     213.pgdg18.04+1             all          PostgreSQL database-cluster manager


netstat -nl | grep postgres
============================


The PostgreSQL server runs on the 5432 port.

::

    netstat -nl | grep postgres

::

    unix  2      [ ACC ]     STREAM     LISTENING     477688   /var/run/postgresql/.s.PGSQL.5432


sudo service postgresql status
============================================

::

    sudo service postgresql status

::

    ● postgresql.service - PostgreSQL RDBMS
       Loaded: loaded (/lib/systemd/system/postgresql.service; enabled; vendor preset: enabled)
       Active: active (exited) since Thu 2020-03-19 10:49:51 CET; 12min ago
      Process: 9343 ExecStart=/bin/true (code=exited, status=0/SUCCESS)
     Main PID: 9343 (code=exited, status=0/SUCCESS)

    mars 19 10:49:51 pvergain-MS-7721 systemd[1]: Starting PostgreSQL RDBMS...
    mars 19 10:49:51 pvergain-MS-7721 systemd[1]: Started PostgreSQL RDBMS.



inxi -Fxxz
=============

::

    inxi -Fxxz

::

    System:    Host: pvergain-MS-7721 Kernel: 4.15.0-91-generic x86_64 bits: 64 compiler: gcc v: 7.4.0 Desktop: Cinnamon 4.4.8
               wm: muffin dm: LightDM Distro: Linux Mint 19.3 Tricia base: Ubuntu 18.04 bionic
    Machine:   Type: Desktop Mobo: MSI model: A88XM-E35 V2 (MS-7721) v: 6.0 serial: <filter> UEFI: American Megatrends v: 6.5
               date: 08/10/2016
    CPU:       Topology: Quad Core model: AMD Athlon X4 860K bits: 64 type: MCP arch: Steamroller rev: 1 L2 cache: 2048 KiB
               flags: lm nx pae sse sse2 sse3 sse4_1 sse4_2 sse4a ssse3 svm bogomips: 29547
               Speed: 1696 MHz min/max: 1700/3700 MHz Core speeds (MHz): 1: 1697 2: 1696 3: 1697 4: 1696
    Graphics:  Device-1: Advanced Micro Devices [AMD/ATI] Ellesmere [Radeon RX 470/480/570/570X/580/580X] vendor: Micro-Star MSI
               driver: amdgpu v: kernel bus ID: 01:00.0 chip ID: 1002:67df
               Display: x11 server: X.Org 1.19.6 driver: amdgpu,ati unloaded: fbdev,modesetting,radeon,vesa
               resolution: 1680x1050~60Hz
               OpenGL: renderer: AMD Radeon RX 470 Graphics (POLARIS10 DRM 3.23.0 4.15.0-91-generic LLVM 9.0.0) v: 4.5 Mesa 19.2.8
               direct render: Yes
    Audio:     Device-1: Advanced Micro Devices [AMD] FCH Azalia vendor: Micro-Star MSI driver: snd_hda_intel v: kernel
               bus ID: 00:14.2 chip ID: 1022:780d
               Device-2: Advanced Micro Devices [AMD/ATI] Ellesmere [Radeon RX 580] vendor: Micro-Star MSI driver: snd_hda_intel
               v: kernel bus ID: 01:00.1 chip ID: 1002:aaf0
               Sound Server: ALSA v: k4.15.0-91-generic
    Network:   Device-1: Realtek RTL8111/8168/8411 PCI Express Gigabit Ethernet vendor: Micro-Star MSI driver: r8169 v: 2.3LK-NAPI
               port: d000 bus ID: 02:00.0 chip ID: 10ec:8168
               IF: enp2s0 state: up speed: 1000 Mbps duplex: full mac: <filter>
    Drives:    Local Storage: total: 1.47 TiB used: 430.41 GiB (28.5%)
               ID-1: /dev/sda vendor: LDLC model: LDLC size: 111.79 GiB speed: 6.0 Gb/s serial: <filter>
               ID-2: /dev/sdb vendor: Western Digital model: WD10EZEX-00WN4A0 size: 931.51 GiB speed: 6.0 Gb/s serial: <filter>
               ID-3: /dev/sdg type: USB vendor: Western Digital model: WD5000LPCX-00VHAT0 size: 465.76 GiB serial: <filter>
    Partition: ID-1: / size: 124.57 GiB used: 114.54 GiB (91.9%) fs: ext4 dev: /dev/sdb3
               ID-2: swap-1 size: 7.96 GiB used: 780 KiB (0.0%) fs: swap dev: /dev/sdb4
    Sensors:   System Temperatures: cpu: 19.9 C mobo: N/A gpu: amdgpu temp: 37 C
               Fan Speeds (RPM): N/A gpu: amdgpu fan: 785
    Info:      Processes: 292 Uptime: 4h 34m Memory: 7.81 GiB used: 4.18 GiB (53.5%) Init: systemd v: 237 runlevel: 5 default: 2
               Compilers: gcc: 7.5.0 alt: 5/7 Shell: zsh v: 5.4.2 running in: python inxi: 3.0.32



Configuring Postgres for Use
==============================

The postgres user
-------------------

While PostgreSQL become installed, a system user account named postgres was also
created with an identical user account in postgres.

**By default, the postgres user account isn't configured with a password**, so it
isn't viable to log into the server the use of the postgres user account without
first creating a password for it.

This postgres account has an all-access pass on your PostgreSQL database server,
permission-wise.

The postgres user account has similarities to the **sa account in SQL server**.

The postgres database
------------------------

**PostgreSQL is installed with a default database postgres**.

For the most part, we use the postgres database for administration functions,
and create new databases on the PostgreSQL server to suit our needs.

The psql Command Line Utility
---------------------------------

PostgreSQL consists of psql, a command line application for managing your
databases and server.

While a GUI-based software such as pgadmin3 is often  less complicated to use
in the daily task, the command line utilty psql is always handy.

psql gives total control of your postgres system from the terminal, together
with the ability to execute SQL queries.

We will use psql to perform our preliminary configuration and to create an
initial database super user.

Login and Connect as Default User
=====================================

For most systems, the default Postgres user is postgres and a password is not
required for authentication.

Thus, to add a password, we must first login and connect as the postgres user.

::

    $ sudo -u postgres psql

::

    [sudo] Mot de passe de pvergain :

::

    psql (12.2 (Ubuntu 12.2-2.pgdg18.04+1))
    Saisissez « help » pour l'aide.

    postgres=#

Changing the Password
=========================

With a connection now established to Postgres at the psql prompt, issue the
ALTER USER command to change the password for the postgres user:

::

    postgres=# alter user postgres password 'myPassword';

::

    ALTER ROLE

If successful, Postgres will output a confirmation of ALTER ROLE as seen above.

Finally, exit the psql client by using the \q command::

    postgres=# \q

You’re all done.

The default postgres user now has a password associated with the account for
use in your other applications.
