.. index::
   pair: EnvDjango ; PostgreSQL

.. _env_django_postgres:

========================================================
.env_django
========================================================


::

    DATABASE_URL=postgresql://<username>:<password>@<ip_postgresql_server>:5432/<database_name>
