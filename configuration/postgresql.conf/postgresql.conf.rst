.. index::
   ! postgresql.conf

.. _postgresql_conf_file:

========================================================
**postgresql.conf** file
========================================================

sudo -u postgres psql -c "SHOW config_file;"
============================================

::

    ✦ ❯ sudo -u postgres psql -c "SHOW config_file;"

::


                   config_file
    -----------------------------------------
     /etc/postgresql/14/main/postgresql.conf
    (1 ligne)


psql -U postgres -h localhost --port 5433 --password
==========================================================

::

    psql -U postgres -h localhost --port 5433 --password


::


    Mot de passe :
    psql (15.3 (Debian 15.3-1.pgdg110+1))
    Connexion SSL (protocole : TLSv1.3, chiffrement : TLS_AES_256_GCM_SHA384, compression : désactivé)
    Saisissez « help » pour l'aide.

    postgres=# SHOW config_file;
                   config_file
    -----------------------------------------
     /etc/postgresql/15/main/postgresql.conf
    (1 ligne)

    postgres=#


Examples
=============

.. literalinclude:: examples/postgresql_conf.txt
