.. index::
   pair: PostgreSQL ; configuration

.. _postgresql_configuration:

========================================================
PostgreSQL **configuration**
========================================================


Sous /etc/postgresql::

::

    ✦ ❯ tree -L 3

::

    ├── 12
    │   └── main
    ├── 13
    │   └── main
    │       ├── conf.d
    │       ├── environment
    │       ├── pg_ctl.conf
    │       ├── pg_hba.conf
    │       ├── pg_ident.conf
    │       ├── postgresql.conf
    │       └── start.conf
    ├── 14
    │   └── main
    │       ├── conf.d
    │       ├── environment
    │       ├── pg_ctl.conf
    │       ├── pg_hba.conf
    │       ├── pg_ident.conf
    │       ├── postgresql.conf
    │       └── start.conf
    └── 15
        └── main
            ├── conf.d
            ├── environment
            ├── pg_ctl.conf
            ├── pg_hba.conf
            ├── pg_ident.conf
            ├── postgresql.conf
            └── start.conf


.. toctree::
   :maxdepth: 3

   postgresql.conf/postgresql.conf
   pg_hba/pg_hba
   env_django/env_django
