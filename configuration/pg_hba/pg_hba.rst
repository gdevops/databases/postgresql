.. index::
   pair: PostgreSQL ; host-based authentication
   ! pg_hba.conf

.. _pg_hba_conf:

=================================================================
**Le fichier pg_hba.conf** (hba **host-based authentication**)
=================================================================


Description
============

L'authentification du client est contrôlée par un fichier, traditionnellement
nommé **pg_hba.conf** et situé dans le répertoire data du groupe de bases
de données, par exemple /usr/local/pgsql/data/pg_hba.conf
(HBA signifie « host-based authentication » : authentification fondée sur l'hôte.)


Un fichier **pg_hba.conf** par défaut est installé lorsque le répertoire data
est initialisé par initdb.

Néanmoins, il est possible de placer le fichier de configuration de
l'authentification ailleurs ; voir le paramètre de configuration hba_file.

Le format général du fichier **pg_hba.conf** est un ensemble d'enregistrements,
un par ligne.
Les lignes vides sont ignorées tout comme n'importe quel texte placé
après le caractère de commentaire #.

Un enregistrement peut être continué sur la ligne suivante en terminant
la ligne avec un antislash. (Les antislashs ne sont pas spéciaux sauf à
la fin d'une ligne.)

Un enregistrement est constitué d'un certain nombre de champs séparés
par des espace et/ou des tabulations. Les champs peuvent contenir des
espaces si la valeur du champ est mise entre guillemets doubles.
Mettre entre guillemets un des mots-clés dans un champ base de données,
utilisateur ou adresse (par exemple, all ou replication) fait que le mot
perd son interprétation spéciale, ou correspond à la base de données,
à l'utilisateur ou à l'hôte ayant ce nom.

La continuaison d'une ligne avec un antislash s'applique même dans le
texte entre guillemets et les commentaires.


Emplacement avec Debian
=========================

- /etc/postgresql/14/main/pg_hba.conf (PostgreSQL 14)


Commandes lsof -i tcp:5432
==============================

::

    lsof -i tcp:5432
