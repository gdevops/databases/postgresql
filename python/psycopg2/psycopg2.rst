.. index::
   pair: psycopg2 ; PostgreSQL
   ! psycopg2

.. _psycopg2:

===============================================================================
**psycopg2** PostgreSQL database adapter for the Python programming language
===============================================================================

- https://github.com/psycopg/psycopg2
- https://www.commentcamarche.net/contents/523-adresse-ip#classe-a

Connection
===============

- https://www.postgresqltutorial.com/postgresql-python/connect/

::

    ipython

::

    Python 3.8.2 (default, Mar  7 2020, 08:47:25)
    Type 'copyright', 'credits' or 'license' for more information
    IPython 7.13.0 -- An enhanced Interactive Python. Type '?' for help.

    In [1]: import psycopg2

    In [2]: conn = psycopg2.connect("dbname=db_intranet host=localhost user=intranet password=xxx")

    In [3]: conn
    Out[3]: <connection object at 0x7f3ce415ec20; dsn: 'user=intranet password=xxx dbname=db_intranet host=localhost', closed: 0>

    In [4]:

Connexion to the local postgres database
===========================================

::

    conn = psycopg2.connect("dbname=postgres user=postgres password='postgres'")



Problem with superset
========================

Résumé des commandes docker-compose
---------------------------------------

- https://github.com/apache/superset/blob/master/superset/db_engine_specs/postgres.py
- https://github.com/apache/superset/issues/15768#issuecomment-987887089

::

    docker-compose -f docker-compose-non-dev.yml up
    date
    docker-compose -f docker-compose-non-dev.yml exec superset pip uninstall -y psycopg2-binary
    docker-compose -f docker-compose-non-dev.yml exec superset pip install psycopg2-binary==2.8.6
    docker-compose -f docker-compose-non-dev.yml restart superset
    date




Versions
========

.. toctree::
   :maxdepth: 3

   versions/versions
