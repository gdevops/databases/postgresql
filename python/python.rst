.. index::
   pair: Python ; PostgreSQL

.. _python_postgresql:

=========================
**Python + PostgreSQL**
=========================

.. toctree::
   :maxdepth: 3

   psycopg2/psycopg2
