.. index::
   pair: Credit ; Dalibo

.. _dalibo:

==================================================
**Dalibo** LICENCE CREATIVE COMMONS BY-NC-SA 2.0
==================================================

.. seealso::

   - https://dalibo.com/formations




Contact Dalibo
=================

.. seealso::

   - https://dalibo.com/formations



:courriel: contact@dalibo.com


Attribution
===========

Pas d’Utilisation Commerciale-Partage dans les Mêmes Conditions

Vous êtes autorisé à:

- Partager, copier, distribuer et communiquer le matériel par tous moyens et
  sous tous formats
- Adapter, remixer, transformer et créer à partir du matériel

Dalibo ne peut retirer les autorisations concédées par la licence tant que vous
appliquezles termes de cette licence selon les conditions suivantes

Attribution
-----------

Vous devez créditer l’œuvre, intégrer un lien vers la licence et indiquer si
des modifications ont été effectuées à l’œuvre.
Vous devez indiquer ces informations par tous les moyens raisonnables, sans
toutefois suggérer que Dalibo vous soutient ou soutient la façon dont vous
avez utilisé ce document.

Pas d’Utilisation Commerciale
---------------------------------

Vous n’êtes pas autorisé à faire un usage commercial de ce document, tout ou
partie du matériel le composant.

Partage dans les Mêmes Conditions
----------------------------------

Dans le cas où vous effectuez un remix, que vous transformez, ou créez à partir
du matériel composant le document original, vous devez diffuser le document
modifié dans les même conditions, c’est à dire avec la même licence avec
laquelle le document original a été diffusé.

Pas de restrictions complémentaires
------------------------------------

Vous n’êtes pas autorisé à appliquer des conditions légales ou des mesures
techniques qui restreindraient légalement autrui à utiliser le document dans
les conditions décrites par la licence.

.. note : Ceci est un résumé de la licence.

Le texte complet est disponible ici_ : https://creativecommons.org/licenses/by-nc-sa/2.0/fr/legalcode

.. _ici: https://creativecommons.org/licenses/by-nc-sa/2.0/fr/legalcode
