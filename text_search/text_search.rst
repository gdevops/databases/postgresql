.. index::
   pair: Full Text Search ; PostgreSQL
   ! Full Text Search

.. _postgresql_full_text_search:

========================================================
PostgreSQL **Full Text Search**
========================================================

.. seealso::

   - https://www.postgresql.org/docs/current/textsearch.html
   - :ref:`django_postgres_search`
