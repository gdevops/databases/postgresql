.. index::
   ! PostgreSQL schemas

.. _postgresql_schemas:

========================================================
**PostgreSQL schemas**
========================================================

- select current_schema();

.. toctree::
   :maxdepth: 3


   commands/commands
