.. index::
   pair: show; search_path
   ! PostgreSQL schemas

.. _schemas_commands:

========================================================
**PostgreSQL schemas commands**
========================================================


PostgreSQL schema operations
===============================

To create a new schema, you use the CREATE SCHEMA statement.
To rename a schema or change its owner, you use the ALTER SCHEMA statement.
To drop a schema, you use the DROP SCHEMA statement.


select current_schema();
==========================

::

    select current_schema();

::

    db_intranet=> select current_schema;
     current_schema
    ----------------
     public
    (1 ligne)

    db_intranet=>



setting the search_path
===========================

::

    SET search_path TO myschema, public;


Update the search_path
=============================

::

    export PGPASSWORD=XXXXXXXXXXX; psql -h localhost -d db_intranet -p 5433 -U intranet -c "ALTER ROLE intranet SET search_path = intranet, public;"


Show the search path
=========================

with psql -c
----------------

::

    ✦ ❯ export PGPASSWORD=XXXXXXXXXXX; psql -U intranet -h localhost -p 5433 -c "\drds"

::

                       Liste des paramètres
       Rôle   | Base de données |           Réglages
    ----------+-----------------+------------------------------
     intranet |                 | search_path=intranet, public
    (1 ligne)



with psql
---------------

::

    export PGPASSWORD=XXXXXXXXXXX; psql -U intranet -h localhost -p 5433

::

    psql (15.2 (Debian 15.2-1.pgdg110+1))
    Connexion SSL (protocole : TLSv1.3, chiffrement : TLS_AES_256_GCM_SHA384, compression : désactivé)
    Saisissez « help » pour l'aide.

    db_intranet=> show search_path;

::


       search_path
    ------------------
     intranet, public
    (1 ligne)


Définir le search_path pour un rôle et/ou une base de données PostgreSQL
===============================================================================

- https://www.docenligne.com/documentation/definir-le-searchpath-pour-un-role-etou-une-base-de-donnees-postgresql.html

Modifier le search_path courant
--------------------------------------

on peut taper la commande suivante::

    SET SEARCH_PATH TO mon_schema,public;


Lors d'une requête, PostgreSQL préfixera d'abord par "mon_schema." puis si
aucune table n'est trouvée dans le schéma "public."

.. warning:: Cette méthode n'est pas persistante et le search_path sera remis à sa valeur
   initiale lors d'un redémarrage du serveur de base de données.


Comment attribuer un search_path à un role PostgreSql spécifique ?
-----------------------------------------------------------------------

Pour conserver le search_path, on doit l'attribuer à un rôle de connexion::

    ALTER ROLE mon_utilisateur SET search_path TO mon_schema,public;

Ou, plus finement, à un rôle ET à une base de données::

    ALTER ROLE mon_utilisateur IN DATABASE ma_bdd SET search_path TO mon_schema,public;
