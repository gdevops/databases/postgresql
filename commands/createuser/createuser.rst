.. index::
   pair: Commands ; createuser
   ! createuser

.. _createuser:

========================================================
**createuser**
========================================================




which createuser
=================

::

    /usr/bin/createuser


createuser --help
====================*

::

    createuser --help

    createuser crée un nouvel rôle PostgreSQL.

    Usage :
      createuser [OPTION]... [NOMROLE]

    Options :
      -c, --connection-limit=N       nombre maximal de connexions pour le rôle
                                     (par défaut sans limite)
      -d, --createdb                 l'utilisateur peut créer des bases de
                                     données
      -D, --no-createdb              le rôle ne peut pas créer de bases de
                                     données (par défaut)
      -e, --echo                     affiche les commandes envoyées au serveur
      -g, --role=ROLE                le nouveau rôle sera un membre de ce rôle
      -i, --inherit                  le rôle hérite des droits des rôles dont il
                                     est membre (par défaut)
      -I, --no-inherit               le rôle n'hérite pas des droits
      -l, --login                    le rôle peut se connecter (par défaut)
      -L, --no-login                 le rôle ne peut pas se connecter
      -P, --pwprompt                 affecte un mot de passe au nouveau rôle
      -r, --createrole               le rôle peut créer des rôles
      -R, --no-createrole            le rôle ne peut pas créer de rôles (par défaut)
      -s, --superuser                le rôle est super-utilisateur
      -S, --no-superuser             le rôle ne sera pas super-utilisateur (par défaut)
      -V, --version              affiche la version puis quitte
      --interactive                  demande le nom du rôle et les attributs
                                     plutôt qu'utiliser des valeurs par défaut
      --replication                le rôle peut initier une connexion de
                                   réplication
      --no-replication             le rôle ne peut pas initier de connexion de
                                   réplication
      -?, --help                 affiche cette aide puis quitte

    Options de connexion :
      -h, --host=HOTE                hôte du serveur de bases de données ou
                                     répertoire des sockets
      -p, --port=PORT                port du serveur de bases de données
      -U, --username=NOMUTILISATEUR  nom de l'utilisateur pour la connexion (pas
                                     celui à créer)
      -w, --no-password             empêche la demande d'un mot de passe
      -W, --password                force la demande d'un mot de passe

    Rapporter les bogues à <pgsql-bugs@lists.postgresql.org>.


Examples
===========


A typical output looks like the following:

::

    createuser -SDr appdev
