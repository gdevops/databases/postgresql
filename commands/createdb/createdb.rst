.. index::
   pair: Commands ; createdb
   ! createdb

.. _createdb:

========================================================
**createdb**
========================================================

- https://pgpedia.info/c/createdb.html

which createdb
================

::

    /usr/bin/createdb



createdb --help
====================

::

    createdb --help


::

    createdb crée une base de données PostgreSQL.

    Usage :
      createdb [OPTION]... [NOMBASE] [DESCRIPTION]

    Options :
      -D, --tablespace=TABLESPACE   tablespace par défaut de la base de données
      -e, --echo                    affiche les commandes envoyées au serveur
      -E, --encoding=ENC            encodage de la base de données
      -l, --locale=LOCALE           paramètre de la locale pour la base de
                                    données
          --lc-collate=LOCALE       paramètre LC_COLLATE pour la base de données
          --lc-ctype=LOCALE         paramètre LC_CTYPE pour la base de données
      -O, --owner=PROPRIÉTAIRE      nom du propriétaire de la nouvelle base de
                                    données
      -T, --template=MODÈLE         base de données modèle à copier
      -V, --version              affiche la version puis quitte
      -?, --help                 affiche cette aide puis quitte

    Options de connexion :
      -h, --host=HOTE               hôte du serveur de bases de données
                                    ou répertoire des sockets
      -p, --port=PORT               port du serveur de bases de données
      -U, --username=NOMUTILISATEUR nom d'utilisateur pour la connexion
      -w, --no-password             empêche la demande d'un mot de passe
      -W, --password                force la demande d'un mot de passe
      --maintenance-db=NOM_BASE    indique une autre base par défaut

    Par défaut, la base de donnée créée porte le nom de l'utilisateur courant.

    Rapporter les bogues à <pgsql-bugs@lists.postgresql.org>.


Examples
===========


::

    createuser -SDr appdev

::

    createdb -O appdev db_dev
