
.. _postgresql_pg_commands:

========================================================
**pg_commands**
========================================================

- https://pgpedia.info/categories/backup.html

::

    ls -als /usr/bin/pg_*

::

     0 lrwxrwxrwx 1 root root    37 févr. 24 16:20 /usr/bin/pg_archivecleanup -> ../share/postgresql-common/pg_wrapper
     0 lrwxrwxrwx 1 root root    37 févr. 24 16:20 /usr/bin/pg_basebackup -> ../share/postgresql-common/pg_wrapper
    12 -rwxr-xr-x 1 root root  9707 nov.   7 13:28 /usr/bin/pg_buildext
     4 -rwxr-xr-x 1 root root  1229 nov.   5  2018 /usr/bin/pg_config
     8 -rwxr-xr-x 1 root root  6262 juin  29  2017 /usr/bin/pg_conftool
    36 -rwxr-xr-x 1 root root 34684 janv. 27 15:05 /usr/bin/pg_createcluster
    24 -rwxr-xr-x 1 root root 23919 nov.  14 13:24 /usr/bin/pg_ctlcluster
     8 -rwxr-xr-x 1 root root  7603 janv. 27 15:05 /usr/bin/pg_dropcluster
     0 lrwxrwxrwx 1 root root    37 févr. 24 16:20 /usr/bin/pg_dump -> ../share/postgresql-common/pg_wrapper
     0 lrwxrwxrwx 1 root root    37 févr. 24 16:20 /usr/bin/pg_dumpall -> ../share/postgresql-common/pg_wrapper
     0 lrwxrwxrwx 1 root root    37 févr. 24 16:20 /usr/bin/pg_isready -> ../share/postgresql-common/pg_wrapper
     8 -rwxr-xr-x 1 root root  5268 févr. 24 15:26 /usr/bin/pg_lsclusters
     0 lrwxrwxrwx 1 root root    37 févr. 24 16:20 /usr/bin/pg_receivewal -> ../share/postgresql-common/pg_wrapper
     0 lrwxrwxrwx 1 root root    37 févr. 24 16:20 /usr/bin/pg_receivexlog -> ../share/postgresql-common/pg_wrapper
     0 lrwxrwxrwx 1 root root    37 févr. 24 16:20 /usr/bin/pg_recvlogical -> ../share/postgresql-common/pg_wrapper
     8 -rwxr-xr-x 1 root root  5887 mars   1  2019 /usr/bin/pg_renamecluster
     0 lrwxrwxrwx 1 root root    37 févr. 24 16:20 /usr/bin/pg_restore -> ../share/postgresql-common/pg_wrapper
    36 -rwxr-xr-x 1 root root 33434 oct.  25 11:50 /usr/bin/pg_upgradecluster
     8 -rwxr-xr-x 1 root root  7859 août   8  2019 /usr/bin/pg_virtualenv

.. toctree::
   :maxdepth: 3
