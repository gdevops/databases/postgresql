.. index::
   pair: PostgreSQL ; commands

.. _postgresql_commands:

========================================================
PostgreSQL **commands**
========================================================

.. toctree::
   :maxdepth: 3

   alter-role/alter-role
   common/common
   createdb/createdb
   create-database/create-database
   createuser/createuser
   create-role/create-role
   create-schema/create-schema
   pg_commands/pg_commands
   pg_dump/pg_dump
   pg_restore/pg_restore
