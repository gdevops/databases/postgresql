.. index::
   pair: Commands ; CREATE SCHEMA
   ! CREATE SCHEMA

.. _create_schema:

================================================================
**CREATE SCHEMA** (An SQL command for defining a new schema)
================================================================

- https://pgpedia.info/c/create-schema.html
- https://www.postgresql.org/docs/current/sql-createschema.html

Description
================

CREATE SCHEMA is a DDL command for defining a new schema.

CREATE SCHEMA was added in PostgreSQL 7.3.

Description
===============

CREATE SCHEMA enters a new schema into the current database.

The schema name must be distinct from the name of any existing schema
in the current database.

A schema is essentially a namespace: it contains named objects (tables,
data types, functions, and operators) whose names can duplicate those
of other objects existing in other schemas.

Named objects are accessed either by “qualifying” their names with the
schema name as a prefix, or by setting a search path that includes the
desired schema(s).

A CREATE command specifying an unqualified object name creates the object
in the current schema (the one at the front of the search path, which
can be determined with the function current_schema).

Optionally, CREATE SCHEMA can include subcommands to create objects within
the new schema.
The subcommands are treated essentially the same as separate commands
issued after creating the schema, except that if the AUTHORIZATION
clause is used, all the created objects will be owned by that user.


Examples
=========

Create a schema for user joe; the schema will also be named joe
------------------------------------------------------------------

::

    CREATE SCHEMA AUTHORIZATION joe;
