.. index::
   pair: help ; pg_restore

.. _pg_restore_help:

======================================================================================
**pg_restore** --help
======================================================================================



pg_restore --help
======================

::

    pg_restore restaure une base de données PostgreSQL à partir d'une archive créée par
    pg_dump.

    Usage :
      pg_restore [OPTION]... [FICHIER]

    Options générales :
      -d, --dbname=NOM             nom de la base de données utilisée pour la
                                   connexion
      -f, --file=NOMFICHIER        nom du fichier de sortie (- pour stdout)
      -F, --format=c|d|t           format du fichier de sauvegarde (devrait être
                                   automatique)
      -l, --list                   affiche la table des matières de l'archive (TOC)
      -v, --verbose                mode verbeux
      -V, --version                affiche la version puis quitte
      -?, --help                   affiche cette aide puis quitte

    Options contrôlant la restauration :
      -a, --data-only              restaure uniquement les données, pas la
                                   structure
      -c, --clean                  nettoie/supprime les objets de la base de
                                   données avant de les créer
      -C, --create                 crée la base de données cible
      -e, --exit-on-error          quitte en cas d'erreur, continue par défaut
      -I, --index=NOM              restaure l'index indiqué
      -j, --jobs=NUMERO            utilise ce nombre de jobs en parallèle pour
                                   la restauration
      -L, --use-list=NOMFICHIER    utilise la table des matières à partir
                                   de ce fichier pour sélectionner/trier
                                   la sortie
      -n, --schema=NOM             restaure uniquement les objets de ce schéma
      -N, --exclude-schema=NOM    ne restaure pas les objets de ce schéma
      -O, --no-owner               omet la restauration des propriétaires des
                                   objets
      -P, --function=NOM(args)     restaure la fonction indiquée
      -s, --schema-only            restaure uniquement la structure, pas les
                                   données
      -S, --superuser=NOM          indique le nom du super-utilisateur à
                                   utiliser pour désactiver les triggers
      -t, --table=NOM              restaure la relation indiquée (table, vue, etc)
      -T, --trigger=NOM            restaure le trigger indiqué
      -x, --no-privileges          omet la restauration des droits sur les objets
                                   (grant/revoke)
      -1, --single-transaction     restaure dans une seule transaction
      --disable-triggers           désactive les triggers en mode de restauration
                                   des données seules
      --enable-row-security        active la sécurité niveau ligne
      --if-exists                  utilise IF EXISTS lors de la suppression des objets
      --no-comments                ne restaure pas les commentaires
      --no-data-for-failed-tables  ne restaure pas les données des tables qui
                                   n'ont pas pu être créées
      --no-publications            ne restaure pas les publications
      --no-security-labels         ne restaure pas les labels de sécurité
      --no-subscriptions           ne restaure pas les souscriptions
      --no-tablespaces             ne restaure pas les affectations de
                                   tablespaces
      --section=SECTION            restaure la section indiquée (pre-data, data
                                   ou post-data)
      --strict-names                 requiert que le motifs de table et/ou schéma
                                     correspondent à au moins une entité de chaque
      --use-set-session-authorization
                                   utilise les commandes SET SESSION AUTHORIZATION
                                   au lieu des commandes ALTER OWNER pour
                                   modifier les propriétaires

    Options de connexion :
      -h, --host=NOMHÔTE           hôte du serveur de bases de données ou
                                   répertoire des sockets
      -p, --port=PORT              numéro de port du serveur de bases de
                                   données
      -U, --username=NOM           se connecter avec cet utilisateur
      -w, --no-password            ne demande jamais le mot de passe
      -W, --password               force la demande du mot de passe (par
                                   défaut)
      --role=NOMROLE               exécute SET ROLE avant la restauration

    Les options -I, -n, -N, -P, -t, -T et --section peuvent être combinées et indiquées
    plusieurs fois pour sélectionner plusieurs objets.

    Si aucun nom de fichier n'est fourni en entrée, alors l'entrée standard est
    utilisée.

    Rapporter les bogues à <pgsql-bugs@lists.postgresql.org>.
