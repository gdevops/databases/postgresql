.. index::
   pair: Commands ; pg_restore
   pair: PostgreSQL ; pg_restore


.. _pg_restore:

======================================================================================
**pg_restore** restore a PostgreSQL database from an archive file created by pg_dump
======================================================================================


Description
==============

- https://pgpedia.info/p/pg_restore.html

A core utility for restoring a PostgreSQL database from an archive file
previously created by pg_dump

pg_restore is a core utility for restoring PostgreSQL database from an
archive file previously created by pg_dump.

pg_restore was added in PostgreSQL 7.1.



.. toctree::
   :maxdepth: 3

   help/help
   man/man
   examples/examples
