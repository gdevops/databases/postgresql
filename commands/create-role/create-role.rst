.. index::
   pair: Commands ; CREATE ROLE
   ! CREATE ROLE

.. _create_role:

================================================================
**CREATE ROLE** (An SQL command for creating a database role)
================================================================

- https://pgpedia.info/c/create-role.html
- https://www.postgresql.org/docs/current/sql-createrole.html

Description
================

CREATE ROLE is a DDL command for creating a database role.

CREATE ROLE was added in PostgreSQL 8.1, replacing the existing CREATE
USER and CREATE GROUP commands (which are retained as aliases of CREATE ROLE).



Description
===============

CREATE ROLE adds a new role to a PostgreSQL database cluster.

A role is an entity that can own database objects and have database
privileges; a role can be considered a “user”, a “group”, or both
depending on how it is used. Refer to Chapter 22 and Chapter 21 for
information about managing users and authentication.

You must have CREATEROLE privilege or be a database superuser to use
this command.

Note that roles are defined at the database cluster level, and so are
valid in all databases in the cluster.
