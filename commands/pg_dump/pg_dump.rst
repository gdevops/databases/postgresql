.. index::
   pair: Commands ; pg_dump
   pair: PostgreSQL ; pg_dump


.. _pg_dump:

======================================================================================
**pg_dump** extract a PostgreSQL database into a script file or other archive file
======================================================================================

- https://pgpedia.info/p/pg_dump.html
- https://www.postgresql.org/docs/current/app-pgdump.html

Description
===========

A core utility for backing up a single PostgreSQL database

pg_dump is a core utility for backing up a single PostgreSQL database
as an SQL script, TAR archive or custom archive file.

pg_dump has been part of PostgreSQL since it was first released.


.. toctree::
   :maxdepth: 3

   help/help
   man/man
   examples/examples
