.. index::
   pair: Commands ; CREATE DATABASE
   ! CREATE DATABASE

.. _create_database:

===========================================================
**CREATE DATABASE** (An SQL command to create a database)
===========================================================

- https://pgpedia.info/c/create-database.html
- https://www.postgresql.org/docs/current/sql-createdatabase.html

Description
================

CREATE DATABASE is a DDL command to create a database.

CREATE DATABASE creates a new PostgreSQL database.

To create a database, you must be a superuser or have the special
CREATEDB privilege. See CREATE ROLE.

By default, the new database will be created by cloning the standard
system database template1.

A different template can be specified by writing TEMPLATE name.

In particular, by writing TEMPLATE template0, you can create a pristine
database (one where no user-defined objects exist and where the system
objects have not been altered) containing only the standard objects
predefined by your version of PostgreSQL.

This is useful if you wish to avoid copying any installation-local objects
that might have been added to template1.
