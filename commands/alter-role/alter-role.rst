.. index::
   pair: Commands ; ALTER ROLE
   ! ALTER ROLE

.. _alter_role:

=================================================================
**ALTER ROLE** (An SQL command for modifying a database role)
=================================================================

- https://pgpedia.info/a/alter-role.html

Description
================
ALTER ROLE is a DDL command for modifying a database role.

ALTER ROLE was added in PostgreSQL 8.1, superseding ALTER USER (which
has been retained as an alias).



Description
=============

- https://www.postgresql.org/docs/current/sql-alterrole.html

ALTER ROLE changes the attributes of a PostgreSQL role.

The first variant of this command listed in the synopsis can change many
of the role attributes that can be specified in CREATE ROLE.

(All the possible attributes are covered, except that there are no options
for adding or removing memberships; use GRANT and REVOKE for that.)

Attributes not mentioned in the command retain their previous settings.

Database superusers can change any of these settings for any role.
Roles having CREATEROLE privilege can change any of these settings except
SUPERUSER, REPLICATION, and BYPASSRLS; but only for non-superuser and
non-replication roles. Ordinary roles can only change their own password.

The second variant changes the name of the role.

Database superusers can rename any role. Roles having CREATEROLE privilege
can rename non-superuser roles. The current session user cannot be renamed.
(Connect as a different user if you need to do that.)
Because MD5-encrypted passwords use the role name as cryptographic salt,
renaming a role clears its password if the password is MD5-encrypted.

The remaining variants change a role's session default for a configuration
variable, either for all databases or, when the IN DATABASE clause is
specified, only for sessions in the named database.
If ALL is specified instead of a role name, this changes the setting for
all roles.
Using ALL with IN DATABASE is effectively the same as using the command
ALTER DATABASE ... SET ....

Whenever the role subsequently starts a new session, the specified value
becomes the session default, overriding whatever setting is present in
postgresql.conf or has been received from the postgres command line.

This only happens at login time; executing SET ROLE or SET SESSION AUTHORIZATION
does not cause new configuration values to be set.

Settings set for all databases are overridden by database-specific settings
attached to a role. Settings for specific databases or specific roles
override settings for all roles.

Superusers can change anyone's session defaults.

Roles having CREATEROLE privilege can change defaults for non-superuser
roles.
Ordinary roles can only set defaults for themselves. Certain configuration
variables cannot be set this way, or can only be set if a superuser
issues the command.
Only superusers can change a setting for all roles in all databases.
