.. index::
   pair: Database ; PostgreSQL
   ! PostgreSQL



.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/databases/postgresql/rss.xml>`_


.. _postgresql_ref:
.. _postgresql:

============================
|Postgresql| **PostgreSQL**
============================


:last stable Postgresql release: :ref:`last_postgresql_version`
:last beta Postgresql release: :ref:`last_postgres_beta_release`
:last dev Postgresql release: :ref:`last_postgres_dev_release`

- https://pgpedia.info/
- https://fr.wikipedia.org/wiki/PostgreSQL
- https://www.postgresql.org/docs/
- https://www.postgresql.org/docs/current/index.html
- https://www.postgresql.org/about/featurematrix/
- https://www.postgresqltutorial.com/
- https://pgxn.org/
- https://x.com/postgrespedia
- https://nitter.poast.org/postgrespedia/rss
- https://dalibo.com/formations
- https://sill.etalab.gouv.fr/software?name=PostgreSQL


.. figure:: images/postgresql.png
   :align: center


.. toctree::
   :maxdepth: 3

   installation/installation
   connexion/connexion
   psql/psql
   service/service
   env/env
   creer-une-base/creer-une-base
   communaute/communaute
   description/description
   configuration/configuration
   upgrade/upgrade
   extensions/extensions
   faqs/faqs

.. toctree::
   :maxdepth: 4

   commands/commands


.. toctree::
   :maxdepth: 3

   credits/credits
   authentification/authentification
   data_types/data_types
   indexes/indexes
   partition/partition
   roles/roles
   schemas/schemas
   python/python
   text_search/text_search
   tips/tips
   tutoriaux/tutoriaux
   glossaire/glossaire
   versions/versions
