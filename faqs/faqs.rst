.. index::
   pair: Postgresql ; FAQ

.. _postgresql_faqs:

=================
Postgresql FAQs
=================


https://popsql.io/learn-sql/postgresql/
========================================

- https://popsql.io/learn-sql/postgresql/


FATAL:  authentification peer échouée pour l'utilisateur « postgres »
=========================================================================

::

    ✦ ❯ psql -U postgres --password
    Mot de passe :
    psql: erreur : la connexion au serveur sur le socket « /var/run/postgresql/.s.PGSQL.5432 »
    a échoué : FATAL:  authentification peer échouée pour l'utilisateur « postgres »
