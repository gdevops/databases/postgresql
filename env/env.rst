.. index::
   pair: Environment ; Variables
   pair: Environment Variable; PGUSER
   pair: Environment Variable; PGPASSWORD
   pair: Environment Variable; PGHOST
   pair: Environment Variable; PGPORT
   pair: Environment Variable; PGDATABASE
   ! PGUSER
   ! PGPASSWORD
   ! PGHOST
   ! PGPORT
   ! PGDATABASE

.. _pg_environment_variables:

=====================================
PostgreSQL Environment variables
=====================================


Setting PostgreSQL environment variables
============================================

::

    export PGUSER=X
    export PGPASSWORD=X
    export PGHOST=X
    export PGPORT=X
    export PGDATABASE=X


Chaine de connexion
======================

::

    export DATABASE_URL=postgresql://<pguser>:<pgpassword>@<pghost>:<pgport>/<pgdatabase>


Unsetting variables
========================

::

    unset PGUSER PGPASSWORD PGHOST PGDATABASE
