.. index::
   pair: Créer ; Base de données
   pair: Créer ; Rôle
   ! Créer une base de données
   ! Créer un rôle

.. _creer_une_base:

========================================================
Créer une base de données PostgreSQL **db_locust**
========================================================


Création de la base de données db_locust
===========================================

::

    CREATE DATABASE db_locust WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'fr_FR.utf8' LC_CTYPE = 'fr_FR.utf8';


::

    CREATE DATABASE

Liste des bases de données avec psql (\l)
--------------------------------------------

::

    psql -U postgres

::

    psql (15.3 (Debian 15.3-1.pgdg120+1))
    Saisissez « help » pour l'aide.


::

    postgres=# \l


::

                                                        Liste des bases de données
        Nom    | Propriétaire | Encodage | Collationnement | Type caract. | Locale ICU | Fournisseur de locale |    Droits d'accès
    -----------+--------------+----------+-----------------+--------------+------------+-----------------------+-----------------------
     db_locust | postgres     | UTF8     | fr_FR.utf8      | fr_FR.utf8   |            | libc                  |
     postgres  | postgres     | UTF8     | fr_FR.UTF-8     | fr_FR.UTF-8  |            | libc                  |
     template0 | postgres     | UTF8     | fr_FR.UTF-8     | fr_FR.UTF-8  |            | libc                  | =c/postgres          +
               |              |          |                 |              |            |                       | postgres=CTc/postgres
     template1 | postgres     | UTF8     | fr_FR.UTF-8     | fr_FR.UTF-8  |            | libc                  | =c/postgres          +
               |              |          |                 |              |            |                       | postgres=CTc/postgres
    (4 lignes)



Création du rôle locust
=========================

::

    CREATE ROLE locust;
    ALTER ROLE locust WITH SUPERUSER INHERIT NOCREATEROLE LOGIN NOREPLICATION NOBYPASSRLS;
    ALTER ROLE locust WITH LOGIN PASSWORD 'locust';


En tant que user linux postgres
-----------------------------------

::

    sudo su - postgres

::

    postgres@uc045:~$ createuser locust



On vérifie que le rôle est bien créé::

    psql -U postgres

::

    psql (15.3 (Debian 15.3-1.pgdg120+1))
    Saisissez « help » pour l'aide.

    postgres=# \du
                                                  Liste des rôles
     Nom du rôle |                                    Attributs                                    | Membre de
    -------------+---------------------------------------------------------------------------------+-----------
     locust      |                                                                                 | {}
     postgres    | Superutilisateur, Créer un rôle, Créer une base, Réplication, Contournement RLS | {}

::

    postgres=# ALTER ROLE locust WITH SUPERUSER INHERIT NOCREATEROLE LOGIN NOREPLICATION NOBYPASSRLS;

::

    ALTER ROLE


On réaffiche::

    postgres=# \du
                                                  Liste des rôles
     Nom du rôle |                                    Attributs                                    | Membre de
    -------------+---------------------------------------------------------------------------------+-----------
     locust      | Superutilisateur                                                                | {}
     postgres    | Superutilisateur, Créer un rôle, Créer une base, Réplication, Contournement RLS | {}


Connexion avec psql
=====================

::

    psql -U locust -d db_locust

::

    psql (15.3 (Debian 15.3-1.pgdg120+1))
    Saisissez « help » pour l'aide.

    db_locust=# \dt
    Aucune relation n'a été trouvée.
    db_locust=#
