.. index::
   pair: Gin ; Index
   ! Gin index


.. _postgresql_gin_index:

========================================================
**PostgreSQL gin index**
========================================================

.. seealso::

   - https://www.postgresql.org/docs/current/gin.html
   - :ref:`django_gin_index`
