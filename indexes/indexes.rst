.. index::
   pair: Indexes ; PostgreSQL
   ! PostgreSQL indexes

.. _postgresql_indexes:

========================================================
**PostgreSQL indexes**
========================================================


.. toctree::
   :maxdepth: 3


   bloom/bloom
   brin/brin
   gin/gin
   gist/gist
