.. index::
   pair: BRIN ; Index
   ! Block Range Index
   ! BRIN


.. _postgresql_brin_index:

========================================================
**PostgreSQL BRIN index** (Block Range Index)
========================================================

.. seealso::

   - https://www.postgresql.org/docs/current/brin.html
   - https://www.postgresql.org/docs/current/brin-intro.html




Introduction
=============

**BRIN** stands for **Block Range Index**.

BRIN is designed for handling very large tables in which certain columns have
some natural correlation with their physical location within the table.

A block range is a group of pages that are physically adjacent in the table;
for each block range, some summary info is stored by the index.

For example, a table storing a store's sale orders might have a date column on
which each order was placed, and most of the time the entries for earlier
orders will appear earlier in the table as well; a table storing a ZIP code
column might have all codes for a city grouped together naturally.
