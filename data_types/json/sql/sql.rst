.. index::
   pair: JSON ; SQL


.. _postgresql_json_sql:

========================================================
**JSON SQL commands**
========================================================


1. Difference between JSON and JSONB
========================================

:source: http://www.silota.com/docs/recipes/sql-postgres-json-data-types.html

The JSON data type is basically a blob that stores JSON data in raw format,
preserving even insignificant things such as whitespace, the order of
keys in objects, or even duplicate keys in objects.

It offers limited querying capabilities, and it's slow because it needs
to load and parse the entire JSON blob each time.

JSONB on the other hand stores JSON data in a custom format that is
optimized for querying and will not reparse the JSON blob each time.

If you know before hand that you will not be performing JSON querying
operations, then use the JSON data type. For all other cases, use JSONB.

The following example demonstrates the difference:


::

    select '{"user_id":1,    "paying":true}'::json, '{"user_id":1, "paying":true}'::jsonb;

::

                  json               |             jsonb
    ---------------------------------+--------------------------------
     {"user_id":1,    "paying":true} | {"paying": true, "user_id": 1}
    (1 ligne)


JSON affichage de champs
==========================

:source: https://public.dalibo.com/exports/formation/manuels/formations/perf2/perf2.handout.pdf


SELECT datas->>'firstName' AS prenom_texte,
datas->'address' AS addr_json
FROM personnes ;
SELECT datas #>> '{address,city}' AS villes FROM personnes ;
SELECT jsonb_array_elements (datas->'phoneNumbers')->>'number' FROM personnes;
Le type json dispose de nombreuses fonctions de manipulation et d’extraction. Les opéra-
teurs ->> et -> renvoient respectivement une valeur au format texte, et au format JSON
