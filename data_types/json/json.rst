.. index::
   pair: JSON types ; PostgreSQL
   ! jsonpath
   ! json
   ! jsonb

.. _postgresql_json_types:
.. _postgresql_json:

========================================================
PostgreSQL **JSON types: jsonpath, json, jsonb**
========================================================

- https://www.postgresql.org/docs/current/datatype-json.html
- https://tools.ietf.org/html/rfc7159
- https://www.postgresql.org/docs/current/datatype-json.html#DATATYPE-JSONPATH
- :ref:`django:django_json_field`

Introduction
=============

**JSON data types** are for storing JSON (JavaScript Object Notation) data, as
specified in `RFC 7159`_.

Such data can also be stored as text, but the JSON data types have the advantage
of enforcing that each stored value is valid according to the JSON rules.

There are also assorted JSON-specific functions and operators available for
data stored in these data types; see Section 9.15.

PostgreSQL offers two types for storing JSON data: json and **jsonb**.

To implement efficient query mechanisms for these data types, PostgreSQL also
provides the jsonpath_ data type described in Section 8.14.6.

The json and jsonb data types accept almost identical sets of values as input.

**The major practical difference is one of efficiency**.

The json data type stores an exact copy of the input text, which processing
functions must reparse on each execution; while jsonb data is stored in a
decomposed binary format that makes it slightly slower to input due to added
conversion overhead, but significantly faster to process, since no reparsing
is needed.

**jsonb also supports indexing, which can be a significant advantage**.


.. _`RFC 7159`: https://tools.ietf.org/html/rfc7159
.. _jsonpath: https://www.postgresql.org/docs/current/datatype-json.html#DATATYPE-JSONPATH


Whats new with JSON in 14
============================

.. toctree::
   :maxdepth: 3

   pg14/pg14


JSON par Dalibo
=================

.. toctree::
   :maxdepth: 3

   dalibo/dalibo

Commandes SQL JSON
===================

.. toctree::
   :maxdepth: 3

   sql/sql


Use cases
=============

- https://kamilachyla.com/en/posts/postgres-json/

.. figure:: images/use_cases.png
   :align: center
