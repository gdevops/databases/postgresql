.. index::
   pair: JSON ; Dalibo


.. _json_dalibo:

========================================================
**JSON dalibo**
========================================================

:source: https://public.dalibo.com/exports/formation/manuels/formations/perf2/perf2.handout.pdf


JSON affichage de champs
==========================

::

    SELECT datas->>'firstName' AS prenom_texte,
    datas->'address' AS addr_json
    FROM personnes ;

::

    SELECT datas #>> '{address,city}' AS villes FROM personnes ;

::

    SELECT jsonb_array_elements (datas->'phoneNumbers')->>'number' FROM personnes;


Le type json dispose de nombreuses fonctions de manipulation et d’extraction.
Les opérateurs ->> et -> renvoient respectivement une valeur au format texte,
et au format JSON
