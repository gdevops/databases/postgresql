.. index::
   pair: Data type ; Array
   ! Array

.. _postgresql_array:

========================================================
PostgreSQL **array** data type
========================================================

.. seealso::

   - https://www.postgresql.org/docs/current/arrays.html
   - :ref:`django_array_field`
