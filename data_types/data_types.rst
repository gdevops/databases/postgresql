.. index::
   pair: Data types ; PostgreSQL

.. _postgresql_data_types:

========================================================
PostgreSQL **data types**
========================================================

.. seealso::

   - https://www.postgresql.org/docs/current/datatype.html

.. toctree::
   :maxdepth: 3


   array/array
   interval/interval
   json/json
   range/range
